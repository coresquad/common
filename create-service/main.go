package main

import (
	"log"
	"os"

	"gitlab.com/coresquad/common/create-service/project"
)

func main() {
	args := os.Args[1:]
	if len(args) == 0 {
		log.Fatal("Please define service name")
	}
	project.CreateProject(args[0])
}
