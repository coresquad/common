package project

import (
	"fmt"
	"os"
	"os/exec"

	"gitlab.com/coresquad/common/create-service/template"

	"github.com/sirupsen/logrus"
)

var (
	exampleFolder    = "example"
	middlewareFolder = "middleware"
	migrationFolder  = "migration"
	modelFolder      = "model"
	protoFolder      = "proto"
	repositoryFolder = "repository"
	scriptFolder     = "script"
	serverFolder     = "server"
	serviceFolder    = "service"
	transportFolder  = "transport"
	utilsFolder      = "utils"
	k8sFolder        = "k8s"
	certFolder       = "cert"
	projectDir       = []string{
		exampleFolder,
		middlewareFolder,
		migrationFolder,
		modelFolder,
		protoFolder,
		repositoryFolder,
		scriptFolder,
		serverFolder,
		serviceFolder,
		transportFolder,
		utilsFolder,
		k8sFolder,
		certFolder,
	}

	// child repo dir
	interfaceFolder = "interface"
	mysqlFolder     = "mysql"
	postgreFolder   = "postgre"
	redisFolder     = "redis"
	childRepo       = []string{interfaceFolder, mysqlFolder, postgreFolder, redisFolder}
)
var l = logrus.New()

func CreateProject(module string) error {
	// logger
	l.Out = os.Stdout

	// path or name root folder
	pathRootFolder := GetRootFolder(module)

	fileDataTemplate := template.Template{
		ModuleName: module,
		FolderName: pathRootFolder,
	}
	fmt.Println(fileDataTemplate)

	// create root folder/dir
	l.Info("Generate Root Project Directory")
	if err := CreateFolder(pathRootFolder); err != nil {
		l.Error("Error Generate Root Project Directory", " ", err.Error())
		return err
	}

	// create root file
	l.Info("Generate root file ", "Module: ", module, " Root DIR: ", pathRootFolder)
	CreateFile(fmt.Sprintf("%s/%s", pathRootFolder, "Makefile"), []byte(template.GenerateRootMakefile(fileDataTemplate)))
	CreateFile(fmt.Sprintf("%s/%s", pathRootFolder, "go.mod"), []byte(template.GetGoMod(fileDataTemplate)))
	CreateFile(fmt.Sprintf("%s/%s", pathRootFolder, ".env.example"), []byte(template.GetExampleEnvFile()))
	// generateProjectDir
	l.Info("Generate Project Directory")
	if err := generateProjectDir(pathRootFolder); err != nil {
		l.Error("Error Generate Project Directory", " ", err.Error())
		return err
	}

	// migration
	var (
		pathMigration = fmt.Sprintf("%s/%s", pathRootFolder, migrationFolder)
		initMigration = fmt.Sprintf("%s/migrator.go", pathMigration)
	)
	l.Info("Create file ", initMigration)
	CreateFile(initMigration, []byte(template.GetMigrator()))

	// server
	var (
		pathServer = fmt.Sprintf("%s/%s", pathRootFolder, serverFolder)
		serverFile = fmt.Sprintf("%s/server.go", pathServer)
	)
	l.Info("Create file ", serverFile)
	CreateFile(serverFile, []byte(template.GetServerFile(fileDataTemplate)))

	// service
	var (
		pathService     = fmt.Sprintf("%s/%s", pathRootFolder, serviceFolder)
		serviceMakefile = fmt.Sprintf("%s/Makefile", pathService)
		serviceFile     = fmt.Sprintf("%s/service.go", pathService)
	)
	l.Info("Create file ", serviceMakefile)
	CreateFile(serviceMakefile, []byte(template.GetServiceMakefile()))
	l.Info("Create file ", serviceFile)
	CreateFile(serviceFile, []byte(template.GetServiceFile(fileDataTemplate)))

	// transport
	var (
		pathTransport      = fmt.Sprintf("%s/%s", pathRootFolder, transportFolder)
		transportClientDir = fmt.Sprintf("%s/%s/client", pathRootFolder, transportFolder)
		transportFile      = fmt.Sprintf("%s/transport.go", pathTransport)
	)
	l.Info("Create file ", transportFile)
	CreateFile(transportFile, []byte(template.GetTransportFile(fileDataTemplate)))

	l.Info("Create dir ", transportClientDir)
	if err := CreateFolder(transportClientDir); err != nil {
		return err
	}

	// repository
	var (
		pathRepo        = fmt.Sprintf("%s/%s", pathRootFolder, repositoryFolder)
		repofactoryFile = fmt.Sprintf("%s/repository.go", pathRepo)
	)
	l.Info("Create file ", repofactoryFile)
	CreateFile(repofactoryFile, []byte(template.GetRepoFactory(fileDataTemplate)))
	// generateSubDirRepo
	if err := generateSubDirRepo(pathRepo); err != nil {
		return err
	}
	var (
		repoMakefile  = fmt.Sprintf("%s/%s/Makefile", pathRepo, interfaceFolder)
		repoInterface = fmt.Sprintf("%s/%s/interface.go", pathRepo, interfaceFolder)
		repoRedis     = fmt.Sprintf("%s/%s/redis.go", pathRepo, redisFolder)
		repoMysql     = fmt.Sprintf("%s/%s/mysql.go", pathRepo, mysqlFolder)
	)
	l.Info("Create file ", repoInterface)
	CreateFile(repoInterface, []byte(template.GetRepositoryInterface()))
	l.Info("Create file ", repoMakefile)
	CreateFile(repoMakefile, []byte(template.GetRepositoryMakefile()))
	l.Info("Create file ", repoRedis)
	CreateFile(repoRedis, []byte(template.GetRepoRedis(fileDataTemplate)))
	l.Info("Create file ", repoMysql)
	CreateFile(repoMysql, []byte(template.GetRepoMysql(fileDataTemplate)))

	// proto
	var (
		pathProtoDir        = fmt.Sprintf("%s/%s", pathRootFolder, protoFolder)
		pathProtofile       = fmt.Sprintf("%s/hallo.proto", pathProtoDir)
		pathGrpcGatewayYaml = fmt.Sprintf("%s/hallo.yaml", pathProtoDir)
		pathProtoMakefile   = fmt.Sprintf("%s/Makefile", pathProtoDir)
	)
	// generate proto file
	l.Info("Create file ", pathProtofile)
	CreateFile(pathProtofile, []byte(template.GetHelloProto(fileDataTemplate)))
	l.Info("Create file ", pathGrpcGatewayYaml)
	CreateFile(pathGrpcGatewayYaml, []byte(template.GetGrpcGatewayYaml(fileDataTemplate)))
	l.Info("Create file ", pathProtoMakefile)
	CreateFile(pathProtoMakefile, []byte(template.GetProtoMakefile(fileDataTemplate)))
	l.Info("Generate file proto + grpc gateway")
	cmdProto := exec.Command("make", "generate")
	cmdProto.Dir = pathProtoDir
	cmdProto.Output()

	// middleware
	var (
		pathMiddlewareDir    = fmt.Sprintf("%s/%s", pathRootFolder, middlewareFolder)
		middHeaderToMetadata = fmt.Sprintf("%s/%s", pathMiddlewareDir, "headerToMetadata.go")
		midrequestId         = fmt.Sprintf("%s/%s", pathMiddlewareDir, "requestIdMiddleware.go")
	)
	l.Info("Create file ", middHeaderToMetadata)
	CreateFile(middHeaderToMetadata, []byte(template.GetHeaderToMetadata(fileDataTemplate)))
	l.Info("Create file ", midrequestId)
	CreateFile(midrequestId, []byte(template.GetUnaryServerInterceptor(fileDataTemplate)))

	// utils
	var (
		constantDirName = "constant"
		pathUtilsDir    = fmt.Sprintf("%s/%s", pathRootFolder, utilsFolder)
		pathUtilsConst  = fmt.Sprintf("%s/%s", pathUtilsDir, constantDirName)
		constantFile    = fmt.Sprintf("%s/%s", pathUtilsConst, "constant.go")
	)
	// create dir utils/constant
	l.Info("Create dir ", pathUtilsConst)
	if err := CreateFolder(pathUtilsConst); err != nil {
		return err
	}
	l.Info("Create file ", constantFile)
	CreateFile(constantFile, []byte(template.GetConstantFile()))

	// script
	var (
		pathScript          = fmt.Sprintf("%s/%s", pathRootFolder, scriptFolder)
		k8sScriptDir        = "k8s"
		pathk8sScriptDir    = fmt.Sprintf("%s/%s", pathScript, k8sScriptDir)
		scriptDockerfile    = fmt.Sprintf("%s/Dockerfile", pathScript)
		scriptBuild         = fmt.Sprintf("%s/build.sh", pathScript)
		scriptDependency    = fmt.Sprintf("%s/dependency.sh", pathScript)
		scriptDeploy        = fmt.Sprintf("%s/deploy.sh", pathScript)
		scriptDockerCompose = fmt.Sprintf("%s/docker-compose.yml", pathScript)
	)
	l.Info("Create file for deploy with docker compose")
	l.Info("Create file ", scriptDockerfile)
	CreateFile(scriptDockerfile, []byte(template.GetScriptDockerfile(fileDataTemplate)))
	l.Info("Create file ", scriptBuild)
	CreateFile(scriptBuild, []byte(template.GetScriptBuild(fileDataTemplate)))
	l.Info("Create file ", scriptDependency)
	CreateFile(scriptDependency, []byte(template.GetScriptDependency()))
	l.Info("Create file ", scriptDeploy)
	CreateFile(scriptDeploy, []byte(template.GetScriptDeploy()))
	l.Info("Create file ", scriptDockerCompose)
	CreateFile(scriptDockerCompose, []byte(template.GetScriptDockerCompose(fileDataTemplate)))

	// script/k8s
	var (
		k8sBuildImage      = fmt.Sprintf("%s/build_image.sh", pathk8sScriptDir)
		k8sBuildBinary     = fmt.Sprintf("%s/build_binary.sh", pathk8sScriptDir)
		k8sBuildDeploy     = fmt.Sprintf("%s/deploy.sh", pathk8sScriptDir)
		k8sBuildDockerfile = fmt.Sprintf("%s/Dockerfile", pathk8sScriptDir)
		k8sPushImage       = fmt.Sprintf("%s/push_image.sh", pathk8sScriptDir)
	)
	l.Info("Create dir ", pathk8sScriptDir)
	if err := CreateFolder(pathk8sScriptDir); err != nil {
		return err
	}
	l.Info("Create file ", k8sBuildImage)
	CreateFile(k8sBuildImage, []byte(template.GetScriptK8SBuildImage()))
	l.Info("Create file ", k8sBuildBinary)
	CreateFile(k8sBuildBinary, []byte(template.GetScriptK8SBuildBinary(fileDataTemplate)))
	l.Info("Create file ", k8sBuildDeploy)
	CreateFile(k8sBuildDeploy, []byte(template.GetScriptK8SDeploy()))
	l.Info("Create file ", k8sBuildDockerfile)
	CreateFile(k8sBuildDockerfile, []byte(template.GetScriptK8SDockerfile(fileDataTemplate)))
	l.Info("Create file ", k8sPushImage)
	CreateFile(k8sPushImage, []byte(template.GetPushImage()))

	// k8s(deployment)
	var (
		pathDirK8sDeploy = fmt.Sprintf("%s/%s", pathRootFolder, k8sFolder)
		k8sService       = fmt.Sprintf("%s/service.yaml", pathDirK8sDeploy)
		k8sDeployment    = fmt.Sprintf("%s/deployment.yaml", pathDirK8sDeploy)
	)
	l.Info("Create file ", k8sService)
	CreateFile(k8sService, []byte(template.Getk8sService(fileDataTemplate)))
	l.Info("Create file ", k8sDeployment)
	CreateFile(k8sDeployment, []byte(template.Getk8SDeployment(fileDataTemplate)))

	// main file
	var (
		mainFile    = fmt.Sprintf("%s/main.go", pathRootFolder)
		jenkinsFile = fmt.Sprintf("%s/Jenkinsfile", pathRootFolder)
		deployNotif = fmt.Sprintf("%s/notif.sh", pathRootFolder)
	)
	l.Info("Create file ", mainFile)
	CreateFile(mainFile, []byte(template.GetMainFile(fileDataTemplate)))
	l.Info("Create file ", jenkinsFile)
	CreateFile(jenkinsFile, []byte(template.GetJenkinsFile(fileDataTemplate)))
	l.Info("Create file ", deployNotif)
	CreateFile(deployNotif, []byte(template.GetDeployNotif()))

	// exec go mod tidy
	l.Info("Go Mod Tidy,please wait...")
	cmd := exec.Command("go", "mod", "tidy")
	cmd.Dir = pathRootFolder
	cmd.Output()
	l.Info("Go Mod Tidy,finish")

	// exec git init
	l.Info("git init,please wait...")
	cmdGitInit := exec.Command("git", "init")
	cmdGitInit.Dir = pathRootFolder
	cmdGitInit.Output()

	return nil
}

// generateProjectDir
func generateProjectDir(rootFolder string) error {
	for _, pj := range projectDir {
		path := fmt.Sprintf("%s/%s", rootFolder, pj)
		l.Info("Create dir ", path)
		err := CreateFolder(path)
		if err != nil {
			return err
		}
	}
	return nil
}

// generateSubDirRepo
func generateSubDirRepo(path string) error {
	for _, pj := range childRepo {
		path := fmt.Sprintf("%s/%s", path, pj)
		l.Info("Create dir ", path)
		err := CreateFolder(path)
		if err != nil {
			return err
		}
	}
	return nil
}
