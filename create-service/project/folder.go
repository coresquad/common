package project

import (
	"io/ioutil"
	"os"
	"strings"
)

// GetRootFolder
// will parse root folder if using
// url module Ex: gitlab.com/aditya37/something
// will return value something
func GetRootFolder(module string) string {
	splits := strings.Split(module, "/")
	return splits[len(splits)-1]
}

// CreateFolder ...
func CreateFolder(folderName string) error {
	err := os.MkdirAll(folderName, os.ModePerm)
	if err != nil {
		return err
	}
	return nil
}

// CreateFile ...
func CreateFile(filename string, data []byte) error {
	err := ioutil.WriteFile(filename, data, 0644)
	return err
}
