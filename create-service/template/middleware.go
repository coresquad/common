package template

func GetHeaderToMetadata(t Template) string {
	tpl := `package middleware

import (
	"context"
	"net/http"
	"strings"

	"{{.ModuleName}}/utils/constant"
	"google.golang.org/grpc/metadata"
)

func HeaderToMetadata(ctx context.Context, r *http.Request) metadata.MD {
	value := make(map[string]string)

	// value request id
	value[constant.REQUEST_ID.ToString()] = r.Header.Get(strings.ToTitle("x-request-id"))

	// header version
	value[constant.REQUEST_VERSION.ToString()] = r.Header.Get(strings.ToTitle("x-request-version"))

	// url path
	value[constant.URL_PATH.ToString()] = r.URL.Path

	// TODO: add another header

	// assign value to metadata...
	md := metadata.New(value)
	return md
}
	`
	return Parse("headerToMetadata.go", tpl, t)
}

// GetUnaryInterceptor
func GetUnaryServerInterceptor(t Template) string {
	tpl := `package middleware

import (
	"context"

	"github.com/google/uuid"
	"{{.ModuleName}}/utils/constant"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

// mapping request id if using grpc
func RequestIdMiddleware() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp any, err error) {
		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return handler(ctx, req)
		}
		// url path
		urlPath := info.FullMethod
		if len(md[constant.URL_PATH.ToString()]) != 0 && md[constant.URL_PATH.ToString()][0] != "" {
			urlPath = md[constant.URL_PATH.ToString()][0]
		}

		// get header request id
		reqUUID := uuid.New().String()
		if len(md[constant.REQUEST_ID.ToString()]) != 0 && md[constant.REQUEST_ID.ToString()][0] != "" {
			reqUUID = md[constant.REQUEST_ID.ToString()][0]
		}

		ctx = context.WithValue(ctx, constant.ContextKeyRequestId.String(), reqUUID)
		ctx = context.WithValue(ctx, constant.ContextKeyUrlPath.String(), urlPath)

		return handler(ctx, req)
	}
}	
	`
	return Parse("requestIdMiddleware.go", tpl, t)
}
