package template

func GetRepositoryInterface() string {
	tmpl := `package _interface

import (
	"database/sql"
	"io"
)

type DBReaderWriter interface {
	io.Closer
	Begin() (*sql.Tx, error)
	Commit(tx *sql.Tx) error
	Rollback(tx *sql.Tx) error
	MysqlClient() *sql.DB
}

type Redis interface {
	io.Closer
}
`
	return tmpl
}

// GetRepoFactory
func GetRepoFactory(t Template) string {
	tmpl := `package repository

import (
	_interface "{{.ModuleName}}/repository/interface"
)

type Repository struct {
	DBReaderWriter _interface.DBReaderWriter
	Redis          _interface.Redis
}
	
type RepoFactory interface {
	InitRepo(*Repository) error
}

var repoInstance *Repository

func NewRepository(repo []RepoFactory) (*Repository, error) {
	if repoInstance != nil {
		return repoInstance, nil
	}
	repoInstance = &Repository{}
	for _, ri := range repo {
		if err := ri.InitRepo(repoInstance); err != nil {
			return nil, err
		}
	}
	return repoInstance, nil
}

func (r *Repository) Close() error {
	r.DBReaderWriter.Close()
	r.Redis.Close()
	return nil
}
	`
	return Parse("repository.go", tmpl, t)
}

// GetRepoRedis
func GetRepoRedis(t Template) string {
	tpl := `package redis

import (
	"context"

	cache "github.com/redis/go-redis/v9"
	"{{.ModuleName}}/repository"
)

type redisClient struct {
	redis *cache.Client
}

type RedisConfig struct {
	Address  string
	Password string
	DB       int
}

func (rf *RedisConfig) InitRepo(repo *repository.Repository) error {
	option := cache.Options{
		Addr:     rf.Address,
		Password: rf.Password,
		DB:       rf.DB,
	}
	client := cache.NewClient(&option)

	// ping
	if err := client.Ping(context.Background()).Err(); err != nil {
		return err
	}

	repo.Redis = &redisClient{
		redis: client,
	}
	return nil
}

func (rc *redisClient) Close() error {
	return rc.redis.Close()
}	
`
	return Parse("redis.go", tpl, t)
}

// GetRepoMysql
func GetRepoMysql(t Template) string {
	tpl := `package mysql

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"{{.ModuleName}}/repository"
	"go.elastic.co/apm/module/apmsql"
	_ "go.elastic.co/apm/module/apmsql/mysql"
)

type mysqlClient struct {
	db *sql.DB
}
type MysqlRepoConf struct {
	Port     int
	Host     string
	Name     string
	Password string
	User     string
}

func (conf *MysqlRepoConf) InitRepo(repo *repository.Repository) error {

	opt := "parseTime=true"
	connURL := fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?%s",
		conf.User,
		conf.Password,
		conf.Host,
		conf.Port,
		conf.Name,
		opt,
	)
	log.Printf(
		"MySQL Connection %s:%s@tcp(%s:%d)/%s",
		conf.User,
		"*************************",
		conf.Host,
		conf.Port,
		conf.Name,
	)
	db, err := apmsql.Open("mysql", connURL)
	if err != nil {
		return err
	}

	db.SetMaxOpenConns(50)
	db.SetMaxIdleConns(50)
	db.SetConnMaxLifetime(5 * time.Minute)

	repo.DBReaderWriter = &mysqlClient{
		db: db,
	}
	return db.Ping()
}

func (mc *mysqlClient) Close() error {
	return mc.db.Close()
}
func (mc *mysqlClient) MysqlClient() *sql.DB {
	if mc.db != nil {
		return mc.db
	}
	return nil
}

func (rw *mysqlClient) Begin() (*sql.Tx, error) {
	return rw.db.Begin()
}

func (rw *mysqlClient) Commit(tx *sql.Tx) error {
	return tx.Commit()
}

func (rw *mysqlClient) Rollback(tx *sql.Tx) error {
	return tx.Rollback()
}
	`

	return Parse("msql.go", tpl, t)
}
