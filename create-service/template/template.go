package template

import (
	"bytes"
	"html/template"
	"log"
	tt "text/template"
)

type Template struct {
	ModuleName string
	FolderName string
}

func Parse(name, tplt string, t Template) string {
	tmpl, err := template.New(name).Parse(tplt)
	if err != nil {
		log.Fatal(err.Error())
	}
	var buffer bytes.Buffer
	if err := tmpl.Execute(&buffer, t); err != nil {
		log.Fatal(err.Error())
	}
	return buffer.String()
}

func ParseText(name, tplt string, t Template) string {
	tmpl, err := tt.New(name).Parse(tplt)
	if err != nil {
		log.Fatal(err.Error())
	}
	var buffer bytes.Buffer
	if err := tmpl.Execute(&buffer, t); err != nil {
		log.Fatal(err.Error())
	}
	return buffer.String()
}
