package template

func GetHelloProto(t Template) string {
	tpl := `syntax="proto3";

package grpc;
option go_package="./;grpc";

import "google/protobuf/empty.proto";

service Something {
	rpc SayHello(google.protobuf.Empty) returns(ResponseSayHello);
}

message ResponseSayHello {
	string hallo = 1;
}
`

	return Parse("hallo.proto", tpl, t)
}

func GetGrpcGatewayYaml(t Template) string {
	tpl := `type: google.api.Service
config_version: 3

http:
 rules:
 - selector: grpc.Something.SayHello
   get: /v1/connection-mode
`
	return Parse("hallo.yaml", tpl, t)
}
