package template

func GetConstantFile() string {
	tpl := `package constant

type HeaderMetadataKey string

const (
	REQUEST_VERSION HeaderMetadataKey = "x-request-version"
	REQUEST_ID      HeaderMetadataKey = "x-request-id"
	URL_PATH        HeaderMetadataKey = "url_path"
)

func (hk HeaderMetadataKey) ToString() string {
	return string(hk)
}

type contextKey string

func (ck contextKey) String() string {
	return string(ck)
}

var (
	ContextKeyRequestId = contextKey("ctxRequestID")
	ContextKeyUrlPath   = contextKey("ctxUrlPath")
)

func ConvertStatusBooleanToInteger(value bool) int32 {
	if !value {
		return 2
	}
	return 1
}
	`
	return tpl
}
