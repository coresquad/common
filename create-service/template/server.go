package template

func GetServerFile(t Template) string {
	tpl := `package server

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	pb "{{.ModuleName}}/proto"
	"{{.ModuleName}}/repository"
	"{{.ModuleName}}/service"
)

type Server struct {
	repo repository.Repository
}

func NewHelloService(
	repo repository.Repository,
) (service.HelloService, error) {

	return &Server{
		repo: repo,
	}, nil
}

func (s *Server) SayHello(context.Context, *empty.Empty) (pb.ResponseSayHello, error) {
	return pb.ResponseSayHello{}, nil
}
`
	return Parse("server.go", tpl, t)
}
