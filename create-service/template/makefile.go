package template

func GenerateRootMakefile(t Template) string {
	tp := `
unitest:
	go test ./repository/mysql/ --cover
	go test ./server/ --cover
	go test ./transport/ --cover
	go test ./repository/query/ --cover
deploy-unitest:
	/usr/local/go/bin/go test ./repository/mysql/ --cover
	/usr/local/go/bin/go test ./server/ --cover
	/usr/local/go/bin/go test ./transport/ --cover
	/usr/local/go/bin/go test ./repository/query/ --cover
	`
	return Parse("Makefile", tp, t)
}

// GetRepositoryMakefile
func GetRepositoryMakefile() string {
	return `mockinterface:
	mockgen -source=interface.go -package=_interface -destination=./mock/interface_mock.go
	`
}

// GetProtoMakefile
func GetProtoMakefile(t Template) string {
	tpl := `generate:
	protoc -I . --go_out=./ --go_opt=paths=source_relative --go-grpc_out=./ --go-grpc_opt=paths=source_relative hallo.proto
	protoc -I . --grpc-gateway_out=logtostderr=true,grpc_api_configuration=hallo.yaml:. hallo.proto
	protoc -I ./ --swagger_out=logtostderr=true,grpc_api_configuration=hallo.yaml:. hallo.proto
gen-proto-rpc:
	protoc -I . --go_out=./ --go_opt=paths=source_relative --go-grpc_out=./ --go-grpc_opt=paths=source_relative hallo.proto
gen-grpc-gateway:
	protoc -I . --grpc-gateway_out=logtostderr=true,grpc_api_configuration=hallo.yaml:. hallo.proto
gen-swagger:
	protoc -I ./ --swagger_out=logtostderr=true,grpc_api_configuration=hallo.yaml:. hallo.proto
	`
	return Parse("Makefile", tpl, t)
}

func GetServiceMakefile() string {
	tpl := `genmockservice:
	mockgen -source=service.go -package=service -destination=service_mock.go
`
	return tpl
}
