package template

func GetTransportFile(t Template) string {
	tpl := `package transport
import (
	"context"
	pb "{{.ModuleName}}/proto"
	"{{.ModuleName}}/service"
	"github.com/golang/protobuf/ptypes/empty"
)

type Transport struct {
	pb.UnimplementedSomethingServer
	serve service.HelloService
}

func NewTransport(
	serve service.HelloService,
) *Transport {
	return &Transport{
		serve: serve,
	}
}
func (t *Transport) SayHello(context.Context, *empty.Empty) (*pb.ResponseSayHello, error) {
	return &pb.ResponseSayHello{}, nil
}
`
	return Parse("transport.go", tpl, t)
}
