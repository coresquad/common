package template

// GetScriptK8SBuildImage
func GetScriptK8SBuildImage() string {
	tmpl := `#!/usr/bin/env bash

set -e
set -o pipefail

if [[ $# -eq 0 ]] 
    then
    echo "Usage build_image.sh [NAMESPACE] [SERVICE] [TAG] [GROUP]"
    exit 1
fi
docker build  --no-cache -f script/k8s/Dockerfile -t  $4/$1/$2:$3 .  
`
	return tmpl
}

// GetScriptK8SBuildBinary
func GetScriptK8SBuildBinary(t Template) string {
	tpl := `#!/usr/bin/env bash

## export go module
export GO111MODULE=on

## export gosumb
export GOSUMDB=off

# for mapping gitlab to private repo
git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"

echo 'Create binary file'
/usr/local/go/bin/go clean && CGO_ENABLED=0 /usr/local/go/bin/go build -o {{.FolderName}}

chmod +x {{.FolderName}}
	`
	return Parse("build_binary.sh", tpl, t)
}

// GetScriptK8SDeploy
func GetScriptK8SDeploy() string {
	tpl := `#!/usr/bin/env bash

set -e
set -o pipefail

if [[ $# -eq 0 ]] 
    then
    echo "Usage deploy.sh [SERVICE] [NAMESPACE] [TAG] [KUBE_NAMESPACE] [GROUP]"
    exit 1
fi

echo "apply deployment"

# kubectl apply -n $3 -f - ===> apply with stdin
cat k8s/deployment.yaml | sed 's/\$SERVICE'"/$1/g" | sed 's/\$NAMESPACE'"/$2/g" | sed 's/\$TAG'"/$3/g" | sed 's/\$GROUP'"/$5/g"| kubectl apply -n $4 -f - --kubeconfig=kubeconfig.conf
kubectl apply -n $4 -f k8s/service.yaml --kubeconfig=kubeconfig.conf
`
	return tpl
}

// GetScriptK8SDockerfile
func GetScriptK8SDockerfile(t Template) string {
	tpl := `# multi stage
FROM alpine:edge
  
WORKDIR /app 
COPY . ./

# install curl
RUN apk --no-cache add curl wget

# grpc probe health check
RUN GRPC_HEALTH_PROBE_VERSION=v0.4.13 && \
    wget -qO/bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-amd64 && \
    chmod +x /bin/grpc_health_probe

# debug multistage
RUN ls -all & pwd
RUN ls /app

ENTRYPOINT ["./{{.FolderName}}"]
`
	return Parse("Dockerfile", tpl, t)
}

// Getk8sService
func Getk8sService(t Template) string {
	yamlString := `kind: Service
apiVersion: v1
metadata:
  name: {{.FolderName}}
  annotations:
    cloud.google.com/app-protocols: '{"grpc":"HTTP2"}'
spec:
  selector:
    app: {{.FolderName}}
  type: NodePort
  ports:
  - name: grpc
    port: 1515
    targetPort: 1515
---
kind: Service
apiVersion: v1
metadata:
  name: {{.FolderName}}-rest
spec:
  selector:
    app: {{.FolderName}}
  type: NodePort
  ports:
  - name: rest
    port: 15155
    targetPort: 1515
`
	return Parse("service.yaml", yamlString, t)
}

// Getk8SDeployment
func Getk8SDeployment(t Template) string {
	yamlString := `apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: {{.FolderName}}
  name: {{.FolderName}}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: {{.FolderName}}
  template:
    metadata:
      labels:
        app: {{.FolderName}}
    spec:
      volumes:
        # mapping volume with secret
        - name: certificate
          secret:
            secretName: service-ca-secret
        - name: tls-certificate
          secret:
            secretName: service-tls-secret
        # temporary volume
        # if pod terminates, all data will be cleared
        - name: workdir
          emptyDir: {}
      containers:
        - name: {{.FolderName}}
          image: dockhub.kotakkode.com/$GROUP/$NAMESPACE/$SERVICE:$TAG
          resources:
            limits:
              memory: 100M
          ports:
            - containerPort: 1515
          # mounting volume
          volumeMounts:
            - name: certificate
              mountPath: /var/ssl/certificate
            - name: tls-certificate
              mountPath: /var/ssl/tls
            - name: workdir
              mountPath: /var/ssl
          # health check and zero downtime
          readinessProbe:
            exec:
              command: ["/bin/grpc_health_probe", "-v","-addr=:1515"]
            # Number of seconds after the container has started before startup
            initialDelaySeconds: 3
            # How often (in seconds) to perform the probe
            periodSeconds: 5
          # health check
          livenessProbe:
            exec:
              command: ["/bin/grpc_health_probe", "-v","-addr=:1515"]
            initialDelaySeconds: 10
            periodSeconds: 15
`
	return Parse("deployment.yaml", yamlString, t)
}

// GetPushImage
func GetPushImage() string {
	tpl := `#!/usr/bin/env bash

set -e
set -o pipefail

if [[ $# -eq 0 ]] 
    then
    echo "Usage push_image.sh [NAMESPACE] [SERVICE] [TAG] [GROUP]"
    exit 1
fi

echo "push image"
docker tag $4/$1/$2:$3 dockhub.kotakkode.com/$4/$1/$2:$3
docker push dockhub.kotakkode.com/$4/$1/$2:$3
docker rmi $4/$1/$2:$3
`
	return tpl
}
