package template

func GetServiceFile(t Template) string {
	tpl := `package service

import (
	"context"
	pb "{{.ModuleName}}/proto"
	"github.com/golang/protobuf/ptypes/empty"
)
type HelloService interface {
	SayHello(context.Context, *empty.Empty) (pb.ResponseSayHello, error)
}
	`
	return Parse("service.go", tpl, t)
}
