package template

func GetScriptDockerfile(t Template) string {
	tpl := `# multi stage
FROM alpine:edge
	
WORKDIR /app 
COPY . ./
# install curl
RUN apk --no-cache add curl

# debug multistage
RUN ls -all & pwd

ENTRYPOINT ["/app/{{.FolderName}}"]
	`
	return Parse("Dockerfile", tpl, t)
}

// GetScriptBuild
func GetScriptBuild(t Template) string {
	tpl := `#!/usr/bin/env bash

## export go module
export GO111MODULE=on

## export gosumb
export GOSUMDB=off

cp -r ./migration ./script/migration
# chmod 600 script

echo 'Create binary file'
/usr/local/go/bin/go clean && CGO_ENABLED=0 /usr/local/go/bin/go build -o {{.FolderName}}
mv {{.FolderName}} ./script/{{.FolderName}}
`

	return Parse("build.sh", tpl, t)
}

// GetScriptDependency
func GetScriptDependency() string {
	tpl := `#!/usr/bin/env bash
git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"
export GO111MODULE=on
export GOSUMDB=off
`
	return tpl
}

// GetScriptDeploy
func GetScriptDeploy() string {
	tpl := `#!/usr/bin/env bash
# # only for testing
# echo 'Create docker image'
# docker build -t $NAMESPACE:$TAG ./script

docker-compose -f ./script/docker-compose.yml up -d --build --always-recreate-deps
echo 'prune dangle image'
docker image prune -a -f
`
	return tpl
}

// GetScriptDockerCompose
func GetScriptDockerCompose(t Template) string {
	yamlString := `version: '3.3'
services:
  {{.FolderName}}:
    container_name: "{{.FolderName}}"
    image: ${NAMESPACE}:${TAG}
    build:
      dockerfile: Dockerfile
      context: .
    volumes:
      - .:/{{.FolderName}}/
    env_file: .env
    ports:
      - "8080:8080"
    dns:
      - 8.8.8.8
      - 8.8.4.4
    # healthcheck
    healthcheck:
      test: curl --fail http://{{.FolderName}}:8080/ || exit 1
      interval: 40s
      timeout: 30s
      retries: 100
      start_period: 60s
    deploy:
      restart_policy:
        condition: on-failure
        max_attempts: 3
      resources:
        limits:
          memory: 100M
networks:
  default:
    external:
      name: backend_networks`
	return Parse("docker-compose.yml", yamlString, t)
}
