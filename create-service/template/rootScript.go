package template

func GetGoMod(t Template) string {
	tpl := `
module {{.ModuleName}}
go 1.21.5

require (
	github.com/aditya37/get-env v0.0.0-20220409152532-eba7a73ece1f
	github.com/aditya37/logger v0.0.0-20230408163358-e529973c88b5
	github.com/golang/mock v1.6.0
	github.com/golang/protobuf v1.5.4
	github.com/google/uuid v1.6.0
	github.com/gorilla/mux v1.8.1
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	github.com/redis/go-redis/v9 v9.4.0
	github.com/rs/cors v1.11.0
	gitlab.com/coresquad/common v0.0.0-20240524073742-928ed0e0de58
	go.elastic.co/apm/module/apmsql v1.15.0
	google.golang.org/grpc v1.64.0
	google.golang.org/protobuf v1.33.0
	github.com/soheilhy/cmux v0.1.5 // indirect
	github.com/sony/gobreaker v0.5.0 // indirect
)
`

	return Parse("go.mod", tpl, t)
}

// GetExampleEnvFile
func GetExampleEnvFile() string {
	tpl := `
CA_CERT="cert/ca-cert.pem"
CLIENT_CERT="cert/client-cert.pem"
CLIENT_KEY="cert/client-key.pem"
SERVER_CERT="cert/server-cert.pem"
SERVER_KEY="cert/server-key.pem"
SERVICE_ADDRESS="0.0.0.0"
SERVICE_PORT=8080
SECURE_SERVER=false

DB_PORT=3306
DB_HOST="127.0.0.1"
DB_NAME=""
DB_PASSWORD="root"
DB_USER="root"
REDIS_HOST="127.0.0.1"
REDIS_DB=0
REDIS_PASSWORD=""
`
	return tpl
}

func GetMainFile(t Template) string {

	tpl := `package main

import (
	"context"
	"crypto/tls"
	"log"
	"time"

	"github.com/aditya37/logger"
	"github.com/gorilla/mux"
	"github.com/rs/cors"

	getenv "github.com/aditya37/get-env"
	"gitlab.com/coresquad/common/microservice"
	"{{.ModuleName}}/middleware"
	pb "{{.ModuleName}}/proto"
	"{{.ModuleName}}/repository"
	"{{.ModuleName}}/repository/mysql"
	"{{.ModuleName}}/repository/redis"
	"{{.ModuleName}}/server"
	"{{.ModuleName}}/transport"
	"{{.ModuleName}}/migration"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
)

type (
	credentialFile struct {
		Cacert     string
		ClientCert string
		ClientKey  string
		ServerCert string
		ServerKey  string
	}
	config struct {
		address            string
		port               int
		dbport             int
		dbhost             string
		dbname             string
		dbpassword         string
		dbuser             string
		sqlDBMigrationPath string
		redisHost          string
		redisDB            int
		redisPassword      string
		isSecure           bool
		credential         credentialFile
	}
)

func main() {
	ctx := context.Background()
	cf := getConfig()

	rpcTransport, repo, err := cf.serveHelloService()
	if err != nil {
		log.Fatal(err)
		return
	}

	// serve rpc transport
	serve, serverTls := cf.serveGRPC(rpcTransport)
	// cors...
	crs := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT"},
		AllowedHeaders:   []string{"*"},
		AllowCredentials: true,
	})

	// init instance of mux route
	r := mux.NewRouter()

	// TODO: add instance and module if want add custom route

	// server builder
	serverBuild := &microservice.ServerBuilder{}
	serverBuild.SetAddress(cf.port, cf.address)
	serverBuild.SetGRPCServer(serve)
	serverBuild.SetMuxRouter(r)
	// set http handler
	httpHandler := []microservice.RegisterHTTPHandler{
		pb.RegisterSomethingHandlerFromEndpoint,
	}
	serverBuild.SetGRPCGatewayHandler(httpHandler)
	serverBuild.SetHandlerHeaderToMetadata(middleware.HeaderToMetadata)
	serverBuild.SetHttpMiddleware(microservice.DefaultHTTPHandler)
	serverBuild.SetHttpMiddleware(crs.Handler)

	// client credential
	// tls for dial grpc clien
	var clientCred credentials.TransportCredentials
	if cf.isSecure {
		clientCred, err = microservice.TLSClientCredentialFromFile(
			cf.credential.Cacert,
			cf.credential.ClientCert,
			cf.credential.ClientKey,
		)
		if err != nil {
			log.Fatal(err)
			return
		}
	}

	// server
	go func() {
		if err := serverBuild.BuildAndRun(clientCred, serverTls); err != nil {
			logger.Error(err)
		}
	}()
	log.Printf("Server running on port %d", cf.port)

	// GracefulShutdown...
	shutdown := microservice.GracefulShutdown(
		ctx,
		2*time.Second,
		map[string]microservice.CloseCallback{
			"close_server": func(ctx context.Context) error {
				serve.GracefulStop()
				return nil
			},
			"close repo": func(ctx context.Context) error {
				return repo.Close()
			},
		},
	)
	<-shutdown
}

// getConfig
func getConfig() *config {
	// credential
	credentialPath := credentialFile{
		Cacert:     getenv.GetString("CA_CERT", "cert/ca-cert.pem"),
		ClientCert: getenv.GetString("CLIENT_CERT", "cert/client-cert.pem"),
		ClientKey:  getenv.GetString("CLIENT_KEY", "cert/client-key.pem"),
		ServerCert: getenv.GetString("SERVER_CERT", "cert/server-cert.pem"),
		ServerKey:  getenv.GetString("SERVER_KEY", "cert/server-key.pem"),
	}
	return &config{
		address:            getenv.GetString("SERVICE_ADDRESS", "127.0.0.1"),
		port:               getenv.GetInt("SERVICE_PORT", 1515),
		dbport:             getenv.GetInt("DB_PORT", 3306),
		dbhost:             getenv.GetString("DB_HOST", "127.0.0.1"),
		dbname:             getenv.GetString("DB_NAME", "db_device_management"),
		dbpassword:         getenv.GetString("DB_PASSWORD", "root"),
		dbuser:             getenv.GetString("DB_USER", "root"),
		sqlDBMigrationPath: "./migration",
		redisHost:          getenv.GetString("REDIS_HOST", "127.0.0.1"),
		redisDB:            getenv.GetInt("REDIS_DB", 0),
		redisPassword:      getenv.GetString("REDIS_PASSWORD", ""),
		isSecure:           getenv.GetBool("SECURE_SERVER", false),
		credential:         credentialPath,
	}
}

// serveDeviceManagementServiceV1..
func (cf *config) serveHelloService() (*transport.Transport, *repository.Repository, error) {
	// mysqldb
	dbRepoConf := mysql.MysqlRepoConf{
		Port:     cf.dbport,
		Host:     cf.dbhost,
		Name:     cf.dbname,
		Password: cf.dbpassword,
		User:     cf.dbuser,
	}

	// redis config
	redisConf := redis.RedisConfig{
		Address:  cf.redisHost,
		Password: cf.redisPassword,
		DB:       cf.redisDB,
	}

	// factory..
	repoFactory := []repository.RepoFactory{
		&dbRepoConf,
		&redisConf,
	}

	repofactory, err := repository.NewRepository(repoFactory)
	if err != nil {
		return nil, nil, err
	}

	// database migrator
	migrator, err := migration.InitMigrator(repofactory.DBReaderWriter.MysqlClient())
	if err != nil {
		return nil, nil, err
	}
	if err := migrator.Up(cf.sqlDBMigrationPath); err != nil {
		return nil, nil, err
	}


	// service or server
	server, _ := server.NewHelloService(*repofactory)
	// transport in here
	rpcTransport := transport.NewTransport(server)

	return rpcTransport, repofactory, nil
}

// serveGRPC
func (cf *config) serveGRPC(transport *transport.Transport) (*grpc.Server, *tls.Config) {
	// rpc middleware
	var (
		opts      []grpc.ServerOption
		tlsConfig *tls.Config
	)

	// validate server use tls or not
	if cf.isSecure {
		cred, tlsConf, err := microservice.TLSCredentialFromFile(cf.credential.Cacert, cf.credential.ServerCert, cf.credential.ServerKey, true)
		if err != nil {
			return nil, nil
		}
		tlsConfig = tlsConf
		opts = append(opts, grpc.Creds(cred))
	}

	// another middleware
	requestIdMiddleware := middleware.RequestIdMiddleware()
	opts = append(opts, grpc.ChainUnaryInterceptor(requestIdMiddleware))

	// register rpc server...
	server := registerGRPC(transport, opts...)

	return server, tlsConfig
}

// registerGRPC
// register rpc server....
func registerGRPC(service pb.SomethingServer, opts ...grpc.ServerOption) *grpc.Server {
	rpcServer := grpc.NewServer(opts...)
	// register proto
	pb.RegisterSomethingServer(rpcServer, service)

	// grpc health check
	microservice.GrpcHealthCheck(rpcServer)

	// grpc reflect
	reflection.Register(rpcServer)
	return rpcServer
}`
	return ParseText("main.go", tpl, t)
}

func GetJenkinsFile(t Template) string {
	tpl := `pipeline {
		agent {
			node {
				label 'Master'
				customWorkspace "workspace/${env.BRANCH_NAME}/src/{{.ModuleName}}"
			}
		}
		environment {
			SERVICE  = "{{.FolderName}}"
			NOTIFDEPLOY = -522638644
			GROUP= "coresquad"
			BOT_TOKEN="1942066429:AAG_XFdhYJPLSvNRuupscAb4IPHa6VRWPec"
			CHAT_ID="-522638644"
		}
		options {
			buildDiscarder(logRotator(daysToKeepStr: env.BRANCH_NAME == 'main' ? '90' : '30'))
		}
		stages {
			stage("Checkout") {
				when {
					anyOf {
						branch 'main'
						branch 'develop'
						branch 'staging'
					}
				}
				steps {
					echo 'Checking out from git'
					checkout scm
					script {
						env.COMMIT_HASH = sh(script: 'git rev-parse --short HEAD', returnStdout: true).trim()
						env.GIT_COMMIT_MSG = sh(script: 'git log -1 --pretty=%B ${GIT_COMMIT}', returnStdout: true).trim()
					}
				}
			}
			stage("prepare") {
				when {
					branch 'develop'
				}
				steps {
					echo "prepare commit ${COMMIT_HASH}"
					withCredentials([file(credentialsId: '9e7c8d99-3ece-49da-bf74-d1c5efd3d14f', variable: 'docker_cred')]) {
						sh 'mv $docker_cred docker_cred.json'
						sh 'chmod 644 docker_cred.json'
						echo "Auth to docker"
						sh 'cat docker_cred.json | jq -r ".kerehore_registry.password" | docker login dockhub.kotakkode.com --username kerehore_registry --password-stdin'
					}
					
				}
			}
			stage("unit test") {
				environment {
					GOPATH = "${env.JENKINS_HOME}/workspace/${env.BRANCH_NAME}"
					PATH = "${env.GOPATH}/bin:${env.PATH}"
				}
				steps {
					echo 'Unit testing'
					sh 'go test ./...'
				}
			}
			stage('Build and deploy') {
				environment {
					// if deploy golang use this....
					GOPATH = "${env.JENKINS_HOME}/workspace/${env.BRANCH_NAME}"
					PATH = "${env.GOPATH}/bin:${env.PATH}"
				}
				stages {
					stage("build binary") {
						when {
							branch 'develop'
						}
						environment {
							NAMESPACE="kinclong-dev"
							TAG = "dev-${COMMIT_HASH}"
						}
						steps {
							// todo: add k8s config
							echo 'Build binary'
							sh "chmod +x script/k8s/build_binary.sh"
							sh './script/k8s/build_binary.sh'
						}
					}
					stage('Build Image') {
						when {
							branch 'develop'
						}
						environment {
							NAMESPACE="kinclong-dev"
							TAG = "dev-${COMMIT_HASH}"
						}
						steps {
							echo 'Build Image'
							sh 'chmod +x ./script/k8s/build_image.sh'
							sh './script/k8s/build_image.sh $NAMESPACE $SERVICE $TAG $GROUP'
						}
					}
					stage('Image Push') {
						when {
							branch 'develop'
						}
						environment {
							NAMESPACE="kinclong-dev"
							TAG = "dev-${COMMIT_HASH}"
						}
						steps {
							echo 'Push Image'
							sh 'chmod +x ./script/k8s/push_image.sh'
							sh './script/k8s/push_image.sh $NAMESPACE $SERVICE $TAG $GROUP'
						}
					}
					stage("Deploy to Dev") {
						when {
							branch 'develop'
						}
						environment {
							NAMESPACE="kinclong-dev"
							TAG = "dev-${COMMIT_HASH}"
						}
						steps {
							withCredentials([file(credentialsId: '346541c1-94ae-40f5-bfc1-dde106267845', variable: 'config')]) {
								sh 'mv $config kubeconfig.conf'
								sh 'chmod 644 kubeconfig.conf'
								sh './script/k8s/deploy.sh $SERVICE $NAMESPACE $TAG default $GROUP'
								sh 'rm kubeconfig.conf'
							}
						}
					}
				}
			}
		}
	post {
		success {
			script {
				sh  'chmod +x notif.sh'
				sh './notif.sh "Success" "Deployment of $SERVICE" was successful "$BOT_TOKEN" "$CHAT_ID"'
			}
		}
		failure {
			script {
				sh  'chmod +x notif.sh'
				sh './notif.sh "Failed" "Deployment of $SERVICE was failed" "$BOT_TOKEN" "$CHAT_ID"'
			}
			
		}
	}
}	
	`
	return ParseText("Jenkinsfile", tpl, t)
}

func GetDeployNotif() string {
	return `#!/bin/bash
	
set -e
set -o pipefail

if [[ $# -eq 0 ]] 
	then
	echo "Usage deploy.sh [STATUS] [MESSAGE] [TOKEN] [MESSAGE ID]"
	exit 1
fi


# get current branch and commit message
BRANCH=${BRANCH_NAME}
COMMIT_MESSAGE=${GIT_COMMIT_MSG}
APPLICATION=${SERVICE}
TIME=$(date -u +"%Y-%m-%d %H:%M:%S UTC")
STATUS=$1
MESSAGE=$2

# Token bot Telegram
BOT_TOKEN=$3
CHAT_ID=$4

# SEND notif by status
if [ "$STATUS" == "Failed" ]; then
MESSAGE_BODY=$(cat <<-END
🚨 *Deploy Notification*

*Status:* $STATUS
*Application:* $APPLICATION
*Branch:* $BRANCH
*Commit Message:* $COMMIT_MESSAGE
*Time:* $TIME
*Error Message:* $MESSAGE

*============*
*BUILD INFO*
*============*
*TAG:* ${BUILD_TAG}
*URL:* ${BUILD_URL}

Please check the logs and resolve the issues.

END
)

else 
MESSAGE_BODY=$(cat <<-END
🚀 *Deploy Notification*

*Status:* $STATUS
*Application:* $APPLICATION
*Branch:* $BRANCH
*Commit Message:* $COMMIT_MESSAGE
*Time:* $TIME
*Message:* $MESSAGE

*============*
*BUILD INFO*
*============*
*TAG:* ${BUILD_TAG}
*URL:* ${BUILD_URL}


Everything is running smoothly!

END
)
fi

# URL API Telegram
URL="https://api.telegram.org/bot$BOT_TOKEN/sendMessage"

# Mengirim pesan ke Telegram
curl -s -X POST $URL -d chat_id=$CHAT_ID -d text="$MESSAGE_BODY" -d parse_mode="Markdown"
`
}
