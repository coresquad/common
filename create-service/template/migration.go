package template

func GetMigrator() string {
	tpl := `package migration

import (
	"database/sql"

	"github.com/pressly/goose"
	_ "go.elastic.co/apm/module/apmsql/mysql"
)

type migrator struct {
	db *sql.DB
}

func InitMigrator(db *sql.DB) (*migrator, error) {
	if err := db.Ping(); err != nil {
		return nil, err
	}
	// Set goose dialect
	if err := goose.SetDialect("mysql"); err != nil {
		return nil, err
	}
	return &migrator{
		db: db,
	}, nil
}

func (m *migrator) filepath(path string) string {
	var filepath string
	if path == "" {
		filepath = "./"
	}
	filepath = path
	return filepath
}

func (m *migrator) Up(path string) error {

	return goose.Up(m.db, m.filepath(path))
}

func (m *migrator) Down(path string) error {
	return goose.Down(m.db, m.filepath(path))
}

func (m *migrator) Upto(path string, version int64) error {
	return goose.UpTo(m.db, m.filepath(path), version)
}`
	return tpl
}
