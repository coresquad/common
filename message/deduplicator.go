package message

import (
	"context"
	"errors"
	"fmt"
	"io"
	"time"

	"github.com/redis/go-redis/v9"
)

type Deduplicator interface {
	io.Closer

	// set message or quee id for validate duplication
	SetMessage(ctx context.Context, key string) error
	IsDuplicate(ctx context.Context, key string) (bool, error)
}

type deduplicator struct {
	KeyPattern string
	redis      *redis.Client
	TTL        time.Duration
}

type Option struct {
	Host       string
	Database   int
	Password   string
	KeyPattern string
	TTL        time.Duration
}

func NewDeduplicator(opt Option) (Deduplicator, error) {

	redisOpt := &redis.Options{
		DB:       opt.Database,
		Addr:     opt.Host,
		Password: opt.Password,
	}
	client := redis.NewClient(redisOpt)
	if err := client.Ping(context.Background()).Err(); err != nil {
		return nil, err
	}

	// default value
	if opt.KeyPattern == "" {
		opt.KeyPattern = "deduplicator"
	}

	// default ttl
	if opt.TTL == 0 {
		opt.TTL = 60 * time.Minute
	}

	return &deduplicator{
		redis:      client,
		KeyPattern: opt.KeyPattern,
		TTL:        opt.TTL,
	}, nil
}

// close...
func (dc *deduplicator) Close() error { return dc.redis.Close() }

// SetMessage...
func (dc *deduplicator) SetMessage(ctx context.Context, key string) error {
	key = fmt.Sprintf("%s-%s", dc.KeyPattern, key)
	err := dc.redis.Set(ctx, key, key, dc.TTL).Err()
	if err != nil {
		return err
	}
	return nil
}

// IsDuplicate
// if return true, message is exist or duplicat
func (dc *deduplicator) IsDuplicate(ctx context.Context, key string) (bool, error) {
	key = fmt.Sprintf("%s-%s", dc.KeyPattern, key)
	err := dc.redis.Get(ctx, key).Err()
	if err != nil {
		if errors.Is(err, redis.Nil) {
			return false, nil
		}
		return false, err
	}
	return true, nil
}
