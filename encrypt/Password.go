package encrypt

import (
	"fmt"

	"golang.org/x/crypto/bcrypt"
)

// hash password
func HashPassword(salt, password string) (string, error) {
	combined := fmt.Sprintf("%s%s", salt, password)
	hash, err := bcrypt.GenerateFromPassword([]byte(combined), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hash), nil
}

// compare hashed password
// password => raw password inputed from user
// salt => salt password from database
// hashPassword => hashed password + salt from db
func ComparePassword(salt, password string, hashPassword []byte) error {
	saltedPassword := fmt.Sprintf("%s%s", salt, password)
	if err := bcrypt.CompareHashAndPassword(hashPassword, []byte(saltedPassword)); err != nil {
		return err
	}
	return nil
}
