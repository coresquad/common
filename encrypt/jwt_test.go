package encrypt

import (
	"errors"
	"reflect"
	"testing"

	getenv "github.com/aditya37/get-env"
	"github.com/golang-jwt/jwt/v4"
	"github.com/stretchr/testify/assert"
	"github.com/undefinedlabs/go-mpatch"
)

func TestGenerateJWT(t *testing.T) {

	t.Run("failed generate token", func(t *testing.T) {
		j := &jwt.Token{}
		var patch *mpatch.Patch
		patch, _ = mpatch.PatchInstanceMethodByName(reflect.TypeOf(j), "SignedString", func(*jwt.Token, interface{}) (string, error) {
			defer patch.Unpatch()
			return "", errors.New("failed create token")
		})

		claims := jwt.MapClaims{}
		_, err := GenerateJWT(claims, 300)
		assert.EqualError(t, err, "failed create token")
	})

	t.Run("success generate token", func(t *testing.T) {
		j := &jwt.Token{}
		var patch *mpatch.Patch
		patch, _ = mpatch.PatchInstanceMethodByName(reflect.TypeOf(j), "SignedString", func(*jwt.Token, interface{}) (string, error) {
			defer patch.Unpatch()
			return "eqqeeee", nil
		})

		claims := jwt.MapClaims{
			"uuid": "XXX-XXX-UUUU",
		}
		token, err := GenerateJWT(claims, 300)
		assert.Nil(t, err)
		assert.Equal(t, "eqqeeee", token)
	})
}

func TestGenerateRefreshToken(t *testing.T) {
	t.Run("failed generate refresh token", func(t *testing.T) {
		j := &jwt.Token{}
		var patch *mpatch.Patch
		patch, _ = mpatch.PatchInstanceMethodByName(reflect.TypeOf(j), "SignedString", func(*jwt.Token, interface{}) (string, error) {
			defer patch.Unpatch()
			return "", errors.New("failed create token")
		})

		claims := jwt.MapClaims{}
		_, err := GenerateRefreshToken(claims, 300)
		assert.EqualError(t, err, "failed create token")
	})
	t.Run("success generate refresh token", func(t *testing.T) {
		j := &jwt.Token{}
		var patch *mpatch.Patch
		patch, _ = mpatch.PatchInstanceMethodByName(reflect.TypeOf(j), "SignedString", func(*jwt.Token, interface{}) (string, error) {
			defer patch.Unpatch()
			return "eqqeeee122sss", nil
		})

		claims := jwt.MapClaims{
			"uuid": "XXX-XXX-UUUU",
		}
		token, err := GenerateRefreshToken(claims, 300)
		assert.Nil(t, err)
		assert.Equal(t, "eqqeeee122sss", token)
	})

}

func TestClaimToken(t *testing.T) {
	t.Run("Negative: error parse", func(t *testing.T) {
		secret := getenv.GetString("JWT_SECRET_REFRESH_TOKEN_KEY", "kinclongSuperSakti")
		token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MjE5NjYyNDksImlhdCI6MTcyMTk2NTk0OSwidXVpZCI6IlhYWC1YWFgtVVVVVSJ9.Ykh4mkilWG_0l-1zX9qCTXFroJHgGtHoIzWBrq6Vr_0"

		var patch *mpatch.Patch
		patch, _ = mpatch.PatchMethod(jwtParser, func(secret, token string) (*jwt.Token, error) {
			defer patch.Unpatch()
			return nil, jwt.ErrInvalidKey
		})
		_, err := ClaimToken(secret, token)
		assert.EqualError(t, err, jwt.ErrInvalidKey.Error())
	})

	t.Run("Negative: error claims not valid", func(t *testing.T) {
		secret := getenv.GetString("JWT_SECRET_REFRESH_TOKEN_KEY", "kinclongSuperSakti")
		token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MjE5NjYyNDksImlhdCI6MTcyMTk2NTk0OSwidXVpZCI6IlhYWC1YWFgtVVVVVSJ9.Ykh4mkilWG_0l-1zX9qCTXFroJHgGtHoIzWBrq6Vr_0"

		var patch *mpatch.Patch
		patch, _ = mpatch.PatchMethod(jwtParser, func(secret, token string) (*jwt.Token, error) {
			defer patch.Unpatch()
			return &jwt.Token{
				Valid:  false,
				Claims: jwt.MapClaims{},
			}, nil
		})

		_, err := ClaimToken(secret, token)
		assert.EqualError(t, err, "token not valid")
	})

	t.Run("Positive: success claim token", func(t *testing.T) {
		secret := getenv.GetString("JWT_SECRET_REFRESH_TOKEN_KEY", "kinclongSuperSakti")
		token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MjE5NjYyNDksImlhdCI6MTcyMTk2NTk0OSwidXVpZCI6IlhYWC1YWFgtVVVVVSJ9.Ykh4mkilWG_0l-1zX9qCTXFroJHgGtHoIzWBrq6Vr_0"

		var patch *mpatch.Patch
		patch, _ = mpatch.PatchMethod(jwtParser, func(secret, token string) (*jwt.Token, error) {
			defer patch.Unpatch()
			return &jwt.Token{
				Valid:  true,
				Claims: jwt.MapClaims{},
			}, nil
		})
		claim, err := ClaimToken(secret, token)
		assert.Nil(t, err)
		assert.Equal(t, jwt.MapClaims{}, claim)
	})
}

func Test_jwtParser(t *testing.T) {

	t.Run("error on parse", func(t *testing.T) {
		// Create a new parser instance
		parser := jwt.NewParser()

		// Define the method to patch
		methodName := "ParseWithClaims"

		// Create a patch for the Parse method
		patch, _ := mpatch.PatchInstanceMethodByName(reflect.TypeOf(parser), methodName, func(jtp *jwt.Parser, tkn string, cms jwt.Claims, clbk jwt.Keyfunc) (*jwt.Token, error) {
			clbk(&jwt.Token{})
			clbk = func(_ *jwt.Token) (interface{}, error) {
				return nil, jwt.ErrInvalidKey
			}
			return nil, jwt.ErrInvalidKey
		})
		defer patch.Unpatch()

		_, err := jwtParser("", "")
		assert.EqualError(t, err, jwt.ErrInvalidKey.Error())
	})

	t.Run("success parse", func(t *testing.T) {
		// Create a new parser instance
		parser := jwt.NewParser()

		// Define the method to patch
		methodName := "ParseWithClaims"

		secret := getenv.GetString("JWT_SECRET_REFRESH_TOKEN_KEY", "kinclongSuperSakti")
		// Create a patch for the Parse method
		patch, _ := mpatch.PatchInstanceMethodByName(reflect.TypeOf(parser), methodName, func(jtp *jwt.Parser, tkn string, cms jwt.Claims, clbk jwt.Keyfunc) (*jwt.Token, error) {
			clbk(&jwt.Token{})
			clbk = func(_ *jwt.Token) (interface{}, error) {
				return []byte(secret), nil
			}
			return &jwt.Token{
				Valid:  true,
				Claims: jwt.MapClaims{},
			}, nil
		})
		defer patch.Unpatch()

		token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MjE5NjYyNDksImlhdCI6MTcyMTk2NTk0OSwidXVpZCI6IlhYWC1YWFgtVVVVVSJ9.Ykh4mkilWG_0l-1zX9qCTXFroJHgGtHoIzWBrq6Vr_0"

		_, err := jwtParser(secret, token)
		assert.Nil(t, err)
	})

}
