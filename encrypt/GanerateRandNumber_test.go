package encrypt

import (
	"testing"
)

func TestGenerateRandNumber(t *testing.T) {
	type args struct {
		digit int
	}
	tests := []struct {
		name      string
		args      args
		wantDigit int
	}{
		{
			name: "Generate 4 digit otp",
			args: args{
				digit: 4,
			},
			wantDigit: 4,
		},
		{
			name: "Generate 6 digit otp",
			args: args{
				digit: 6,
			},
			wantDigit: 6,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GenerateRandNumber(tt.args.digit)

			if len(got) != tt.wantDigit {
				t.Errorf("GenerateRandNumber() = %v, want %v", got, tt.wantDigit)
			}
		})
	}
}
