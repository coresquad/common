package encrypt

import (
	"math/rand"
)

func GenerateRandNumber(digit int) string {
	characters := "0123456789"
	otp := make([]byte, digit)

	for i := range otp {
		otp[i] = characters[rand.Intn(len(characters))]
	}

	return string(otp)
}
