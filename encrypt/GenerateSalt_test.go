package encrypt

import (
	"crypto/rand"
	"errors"
	"testing"

	"github.com/undefinedlabs/go-mpatch"
)

func TestGenerateSalt(t *testing.T) {
	tests := []struct {
		name    string
		want    string
		mock    func()
		wantErr bool
	}{
		{
			name: "Positive: generate salt",
			want: "cGFzc3dvcmRzYXR1ZHVhMQ==",
			mock: func() {
				var patch *mpatch.Patch
				patch, _ = mpatch.PatchMethod(rand.Read, func(b []byte) (int, error) {
					defer patch.Unpatch()
					copy(b, []byte("passwordsatudua1"))
					return 16, nil
				})
			},
			wantErr: false,
		},
		{
			name: "Negative: error generate salt",
			want: "",
			mock: func() {
				var patch *mpatch.Patch
				patch, _ = mpatch.PatchMethod(rand.Read, func(b []byte) (int, error) {
					defer patch.Unpatch()
					return 0, errors.New("failed generate salt")
				})
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()
			got, err := GenerateSalt()
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateSalt() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GenerateSalt() = %v, want %v", got, tt.want)
			}
		})
	}
}
