package encrypt

import (
	"errors"
	"time"

	getenv "github.com/aditya37/get-env"
	"github.com/aditya37/logger"
	"github.com/golang-jwt/jwt/v4"
)

var (
	ErrTokenNotValid = errors.New("token not valid")
	ErrTokenExpired  = errors.New("Token is expired")
)

// What is JWT Issued at (iat)? In the JSON Web Token (JWT) standard,
// the "iat" (issued at) claim is a timestamp
// that indicates the time at which the JWT was issued
func GenerateJWT(payload jwt.MapClaims, ttl time.Duration) (string, error) {
	jwtKey := getenv.GetString("JWT_SECRET_KEY", "but0ijo")

	// default value
	payload["exp"] = time.Now().UTC().Add(ttl * time.Second).Unix()
	payload["iat"] = time.Now().UTC().Unix()

	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, payload).SignedString([]byte(jwtKey))
	if err != nil {
		return "", err
	}
	return token, nil
}

// GenerateRefreshToken
func GenerateRefreshToken(payload jwt.MapClaims, ttl time.Duration) (string, error) {
	jwtKey := getenv.GetString("JWT_SECRET_REFRESH_TOKEN_KEY", "kinclongSuperSakti")

	// default value
	payload["exp"] = time.Now().UTC().Add(ttl * time.Second).Unix()
	payload["iat"] = time.Now().UTC().Unix()

	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, payload).SignedString([]byte(jwtKey))
	if err != nil {
		return "", err
	}
	return token, nil
}

// ClaimToken
func ClaimToken(secret, token string) (jwt.MapClaims, error) {
	parse, err := jwtParser(secret, token)
	if err != nil {
		logger.Error(err)
		return jwt.MapClaims{}, err
	}

	claims := parse.Claims.(jwt.MapClaims)
	if !parse.Valid {
		return jwt.MapClaims{}, ErrTokenNotValid
	}

	return claims, nil
}

func jwtParser(secret, token string) (*jwt.Token, error) {
	keyFunction := func(t *jwt.Token) (interface{}, error) {
		return []byte(secret), nil
	}
	return jwt.NewParser().Parse(token, keyFunction)
}
