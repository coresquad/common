package encrypt

import (
	"testing"

	"github.com/undefinedlabs/go-mpatch"
	"golang.org/x/crypto/bcrypt"
)

func TestHashPassword(t *testing.T) {
	type args struct {
		salt     string
		password string
	}
	tests := []struct {
		name    string
		args    args
		mock    func()
		want    string
		wantErr bool
	}{
		{
			name: "Negative: failed hash password",
			args: args{
				salt:     "cGFzc3dvcmRzYXR1ZHVhMQ==",
				password: "test12345678",
			},
			mock: func() {
				var patch *mpatch.Patch
				patch, _ = mpatch.PatchMethod(bcrypt.GenerateFromPassword, func(p []byte, length int) ([]byte, error) {
					defer patch.Unpatch()
					return nil, bcrypt.ErrMismatchedHashAndPassword
				})
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Positive: success hash password",
			args: args{
				salt:     "cGFzc3dvcmRzYXR1ZHVhMQ==",
				password: "test12345678",
			},
			mock: func() {
				var patch *mpatch.Patch
				patch, _ = mpatch.PatchMethod(bcrypt.GenerateFromPassword, func(p []byte, length int) ([]byte, error) {
					defer patch.Unpatch()
					return []byte("$2a$10$pLk.YhXI5A2lCDN83hoR9u4Wvrv4oSvD1GwgGeiafjLgMEWVuNRu2"), nil
				})
			},
			want:    "$2a$10$pLk.YhXI5A2lCDN83hoR9u4Wvrv4oSvD1GwgGeiafjLgMEWVuNRu2",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()
			got, err := HashPassword(tt.args.salt, tt.args.password)
			if (err != nil) != tt.wantErr {
				t.Errorf("HashPassword() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("HashPassword() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestComparePassword(t *testing.T) {
	type args struct {
		salt         string
		password     string
		hashPassword []byte
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Positive: valid password",
			args: args{
				salt:         "cGFzc3dvcmRzYXR1ZHVhMQ==",
				password:     "test12345678",
				hashPassword: []byte("$2a$10$pLk.YhXI5A2lCDN83hoR9u4Wvrv4oSvD1GwgGeiafjLgMEWVuNRu2"),
			},
			wantErr: false,
		},
		{
			name: "Negative: failed compare password",
			args: args{
				salt:         "cGFzc3dvcmRzYXR1ZHVhMQ==",
				password:     "test1234567",
				hashPassword: []byte("$2a$10$pLk.YhXI5A2lCDN83hoR9u4Wvrv4oSvD1GwgGeiafjLgMEWVuNRu2"),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ComparePassword(tt.args.salt, tt.args.password, tt.args.hashPassword); (err != nil) != tt.wantErr {
				t.Errorf("ComparePassword() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
