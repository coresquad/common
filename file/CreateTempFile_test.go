package file

import (
	"os"
	"reflect"
	"testing"

	"github.com/undefinedlabs/go-mpatch"
)

func TestCreateTempFile(t *testing.T) {
	type args struct {
		filename string
		value    []byte
	}
	tests := []struct {
		name    string
		args    args
		want    *TempFileInfo
		mock    func()
		wantErr bool
	}{
		{
			name: "Negative: Failed create temp",
			args: args{
				filename: "image.png",
				value:    []byte{},
			},
			want: &TempFileInfo{},
			mock: func() {
				var (
					patchTmp *mpatch.Patch
				)
				patchTmp, _ = mpatch.PatchMethod(os.CreateTemp, func(dir string, pattern string) (*os.File, error) {
					patchTmp.Unpatch()
					return nil, os.ErrInvalid
				})
			},
			wantErr: true,
		},
		{
			name: "Negative: Failed write temp file",
			args: args{
				filename: "image.png",
				value:    []byte{},
			},
			want: &TempFileInfo{},
			mock: func() {
				var (
					patchTmp *mpatch.Patch
				)
				f := &os.File{}
				patchTmp, _ = mpatch.PatchInstanceMethodByName(reflect.TypeOf(f), "Write", func(*os.File, []uint8) (int, error) {
					defer patchTmp.Unpatch()
					return 0, os.ErrProcessDone
				})

			},
			wantErr: true,
		},
		{
			name: "Positve: success",
			args: args{
				filename: "image.png",
				value:    []byte(PNGImage),
			},
			want: &TempFileInfo{},
			mock: func() {
				var (
					patchTmp *mpatch.Patch
				)
				f := &os.File{}
				patchTmp, _ = mpatch.PatchInstanceMethodByName(reflect.TypeOf(f), "Write", func(ff *os.File, l []uint8) (int, error) {
					defer patchTmp.Unpatch()
					return 0, nil
				})

			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()
			_, err := CreateTempFile(tt.args.filename, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateTempFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

		})
	}
}
