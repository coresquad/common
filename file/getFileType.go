package file

import (
	"errors"
	"net/http"
	"strings"
)

var (
	MimeTypeToFileType = map[string]string{
		"text/plain":                        "txt",
		"text/html":                         "html",
		"text/css":                          "css",
		"text/javascript":                   "js",
		"image/jpeg":                        "jpg",
		"image/png":                         "png",
		"image/gif":                         "gif",
		"image/svg+xml":                     "svg",
		"audio/mpeg":                        "mp3",
		"audio/ogg":                         "ogg",
		"audio/wav":                         "wav",
		"video/mp4":                         "mp4",
		"video/ogg":                         "ogv",
		"video/webm":                        "webm",
		"application/json":                  "json",
		"application/xml":                   "xml",
		"application/pdf":                   "pdf",
		"application/zip":                   "zip",
		"application/x-www-form-urlencoded": "form",
		"multipart/form-data":               "form",
		"multipart/alternative":             "alt",
	}
	ErrFileCantEmpty = errors.New("file cant empty")
)

func GetFileType(val []byte) (string, error) {

	if len(val) == 0 {
		return "", ErrFileCantEmpty
	}

	// get mime type
	contentType := http.DetectContentType(val)

	// cleaning charset from mimetype
	if semicolonIndex := strings.Index(contentType, ";"); semicolonIndex != -1 {
		contentType = contentType[:semicolonIndex]
	}
	return MimeTypeToFileType[contentType], nil
}
