package file

import (
	"fmt"
	"os"
)

type TempFileInfo struct {
	Name string
	Size int64
}

func CreateTempFile(filename string, value []byte) (*TempFileInfo, error) {
	pattern := fmt.Sprintf("*-%s", filename)
	file, err := os.CreateTemp("", pattern)
	if err != nil {
		return &TempFileInfo{}, err
	}

	// write file
	if _, err := file.Write(value); err != nil {
		return &TempFileInfo{}, err
	}

	// get file stat or size
	fileInfo, _ := file.Stat()
	return &TempFileInfo{
		Name: file.Name(),
		Size: fileInfo.Size(),
	}, nil
}
