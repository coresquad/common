package microservice

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

// function for handle close coonection
type CloseCallback func(ctx context.Context) error

func GracefulShutdown(ctx context.Context, timeout time.Duration, closeCallback map[string]CloseCallback) <-chan struct{} {
	wait := make(chan struct{})
	go func() {
		// dump triggered terminal signal
		termSignal := make(chan os.Signal, 1)

		// terminal signal
		signal.Notify(termSignal, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
		log.Printf("shuting down with signal: %s", <-termSignal)

		// close or shutdown timeout
		// if timeout reacher will force shutdown
		shutdownTimeout := time.AfterFunc(timeout, func() {
			log.Printf("timeout %d ms has been elapsed, force exit", timeout.Milliseconds())
			os.Exit(0)
		})
		defer shutdownTimeout.Stop()

		// add wait group
		// for waiting goroutine proccess cleanin or close
		var wg sync.WaitGroup
		// iterate closeCallback
		for key, clbk := range closeCallback {

			wg.Add(1)

			// variable for go routinr...
			_key := key
			_clbk := clbk

			// do close async for reduce time...
			go func() {
				defer wg.Done()
				// print...
				log.Printf("shuting down %s ", _key)
				if err := _clbk(ctx); err != nil {
					msg := fmt.Sprintf("failed shuting down %s error %s", _key, err.Error())
					log.Println(msg)
					return
				}
				log.Printf("%s was shutdown gracefully", _key)
			}()
		}
		wg.Wait()
		close(wait)
	}()

	return wait
}
