package microservice

import (
	"context"
	"fmt"
	"reflect"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type (
	encoder        func(context.Context, interface{}) (interface{}, error)
	optionMetadata func(context.Context, *metadata.MD) context.Context
	Requester      func(context.Context, interface{}) (interface{}, error)
	Middleware     func(Requester) Requester
)

type gRPCUnaryDial struct {
	client   *grpc.ClientConn
	method   string
	request  encoder
	response encoder
	reply    reflect.Type
	opt      []optionMetadata
}

func RPCUnaryDialer(c *grpc.ClientConn, service, method string, protoReply interface{}, opt ...optionMetadata) *gRPCUnaryDial {
	return &gRPCUnaryDial{
		client:   c,
		method:   fmt.Sprintf("/%s/%s", service, method),
		request:  requestDecoder,
		response: responseEncoder,
		reply: reflect.TypeOf(
			reflect.Indirect(reflect.ValueOf(protoReply)).Interface(),
		),
		opt: opt,
	}
}

// request and response encoder
func requestDecoder(ctx context.Context, r interface{}) (interface{}, error)  { return r, nil }
func responseEncoder(ctx context.Context, r interface{}) (interface{}, error) { return r, nil }

// Request...
// dial and request to rpc server
func (gd gRPCUnaryDial) Request() Requester {
	return func(ctx context.Context, i interface{}) (interface{}, error) {

		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		req, err := gd.request(ctx, i)
		if err != nil {
			return nil, err
		}

		// assign metadata
		md := &metadata.MD{}
		for _, m := range gd.opt {
			ctx = m(ctx, md)
		}

		// header
		var header metadata.MD

		// reply in invoke rpc
		reply := reflect.New(gd.reply).Interface()
		if err := gd.client.Invoke(
			ctx,
			gd.method,
			req,
			reply,
			grpc.Header(&header),
		); err != nil {
			return nil, err
		}

		// decode response
		resp, err := gd.response(ctx, reply)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}
