package microservice

import (
	"crypto/tls"
	"crypto/x509"
	"os"

	"google.golang.org/grpc/credentials"
)

// TLSClientCredentialFromFile...
func TLSClientCredentialFromFile(cacert, svccert, key string) (credentials.TransportCredentials, error) {
	certPair, err := tls.LoadX509KeyPair(svccert, key)
	if err != nil {
		return nil, err
	}
	rawCaCert, err := os.ReadFile(cacert)
	if err != nil {
		return nil, err
	}
	creds, _ := tlsCredential(rawCaCert, certPair, false)
	return creds, nil
}

// TLSCredentialFromFile
func TLSCredentialFromFile(cacert, svccert, key string, isMutual bool) (credentials.TransportCredentials, *tls.Config, error) {
	certPair, err := tls.LoadX509KeyPair(svccert, key)
	if err != nil {
		return nil, nil, err
	}
	rawCaCert, err := os.ReadFile(cacert)
	if err != nil {
		return nil, nil, err
	}

	cred, tlsConf := tlsCredential(rawCaCert, certPair, isMutual)
	return cred, tlsConf, nil
}

// tlsCredential...
func tlsCredential(caCert []byte, cert tls.Certificate, isMutual bool) (credentials.TransportCredentials, *tls.Config) {
	caPool := x509.NewCertPool()
	caPool.AppendCertsFromPEM(caCert)

	tlsConf := &tls.Config{
		Certificates: []tls.Certificate{cert},
		NextProtos:   []string{"h2"},
		ClientCAs:    caPool,
		RootCAs:      caPool,
	}

	if isMutual {
		tlsConf.ClientAuth = tls.RequireAndVerifyClientCert
	}

	cerds := credentials.NewTLS(tlsConf)

	return cerds, tlsConf
}
