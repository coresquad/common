package microservice

import (
	"context"
	"errors"
	"log"
	"time"

	"github.com/sony/gobreaker"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// setting
func CircuitBreakerSetting(name string) gobreaker.Settings {
	return gobreaker.Settings{
		Name:          name,
		MaxRequests:   10,
		Interval:      10 * time.Second,
		Timeout:       5 * time.Second,
		ReadyToTrip:   readyToTrip,
		OnStateChange: onStateChange,
		IsSuccessful:  isSuccessful,
	}
}

// middleware
func CircuitBreakerMiddleware(cb *gobreaker.CircuitBreaker) Middleware {
	return func(next Requester) Requester {
		return func(ctx context.Context, i interface{}) (interface{}, error) {
			// execute breaker
			execute, err := cb.Execute(func() (interface{}, error) {
				return next(ctx, i)
			})
			if err != nil {
				if errors.Is(err, gobreaker.ErrOpenState) {
					return nil, status.Error(codes.DeadlineExceeded, err.Error())
				}
				return nil, err
			}
			return execute, nil
		}
	}
}

// onStateChange
// state change from close to open or to another state...
func onStateChange(name string, from, to gobreaker.State) {
	log.Printf("circuit breaker: name %s from %v to %v", name, from, to.String())
}

// readyToTrip..
func readyToTrip(counts gobreaker.Counts) bool {
	failureRatio := float64(counts.TotalFailures) / float64(counts.Requests)
	log.Printf("CB Request: %v total failure: %v failure ration %f \n", counts.Requests, counts.TotalFailures, failureRatio)
	return counts.Requests >= 10 && failureRatio >= 0.6
}

func isSuccessful(err error) bool {
	// for ignore some error
	// can add error exception
	if err != nil {
		return false
	} else {
		return true
	}
}
