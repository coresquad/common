package microservice

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"strings"

	"github.com/aditya37/logger"
	"github.com/gorilla/mux"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/soheilhy/cmux"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	rpc_health "google.golang.org/grpc/health"
	healthpb "google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/metadata"
)

// create server for grpc and http using grpc-gateway
type (
	// grpc gateway runtime
	RegisterHTTPHandler func(ctx context.Context, mux *runtime.ServeMux, endpoint string, opts []grpc.DialOption) error

	// custom http middlewate...
	HTTPMiddleware func(handler http.Handler) http.Handler

	// handler for assign or convert http header to middleware
	RestHeaderToMetadata func(ctx context.Context, r *http.Request) metadata.MD
)

// default http middleware
func DefaultHTTPHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/" {
			w.WriteHeader(http.StatusOK)
			w.Write([]byte("Health Check Passed"))
		} else {
			handler.ServeHTTP(w, r)
		}
	})
}

// server builder
type ServerBuilder struct {
	// service address
	address string

	// instance gRPC Server
	rpcServer *grpc.Server

	// convert header to metadata gRPC
	headerToMetadata RestHeaderToMetadata

	// gRPC Dial Option
	rpcDialOpt []grpc.DialOption

	// grpc gateway runtime
	grpcGatewayHandler []RegisterHTTPHandler

	// http middleware handler
	httpMiddleware []HTTPMiddleware

	// instance router mux if want add some route
	// without create proto and register gRPC-Gateway
	muxRouter *mux.Router
}

// set port
func (sb *ServerBuilder) SetAddress(port int, address string) *ServerBuilder {
	if port == 0 {
		port = 1515
	}
	if address == "" {
		address = "127.0.01"
	}
	sb.address = fmt.Sprintf("%s:%d", address, port)
	return sb
}

// set rpc server
func (sb *ServerBuilder) SetGRPCServer(srv *grpc.Server) *ServerBuilder {
	sb.rpcServer = srv
	return sb
}

// middleware convert header rest to metadata
func (sb *ServerBuilder) SetHandlerHeaderToMetadata(h func(ctx context.Context, r *http.Request) metadata.MD) *ServerBuilder {
	sb.headerToMetadata = h
	return sb
}

// set rpc dial option
func (sb *ServerBuilder) SetRPCDialOpt(opt ...grpc.DialOption) *ServerBuilder {
	sb.rpcDialOpt = append(sb.rpcDialOpt, opt...)
	return sb
}

// set grpcGatewayHandler
func (sb *ServerBuilder) SetGRPCGatewayHandler(h []RegisterHTTPHandler) *ServerBuilder {
	sb.grpcGatewayHandler = h
	return sb
}

// set http middleware
func (sb *ServerBuilder) SetHttpMiddleware(mid ...HTTPMiddleware) *ServerBuilder {
	sb.httpMiddleware = append(sb.httpMiddleware, mid...)
	return sb
}

// set mux router
func (sb *ServerBuilder) SetMuxRouter(r *mux.Router) *ServerBuilder {
	sb.muxRouter = r
	return sb
}

// build
func (sb *ServerBuilder) BuildAndRun(creds credentials.TransportCredentials, tlsConf *tls.Config) error {
	var (
		ctx        = context.Background()
		httpServer = &http.Server{}
		tlsConfig  = &tls.Config{}
	)

	// tcp listener
	listener, err := net.Listen("tcp", sb.address)
	if err != nil {
		return err
	}

	// multiplexer listener....
	mxRpc, mxRest, multiplexer, err := multiplexListener(listener)
	if err != nil {
		return err
	}

	// grpc gateway runtime
	httpHeaderMetadata := runtime.WithMetadata(sb.headerToMetadata)
	marshalOption := runtime.WithMarshalerOption(
		runtime.MIMEWildcard,
		&runtime.JSONPb{OrigName: true, EmitDefaults: true},
	)
	rpcGateway := runtime.NewServeMux(
		httpHeaderMetadata,
		marshalOption,
	)

	// grpc dial option with tls or not
	if creds != nil {
		// append dial option with creds
		sb.rpcDialOpt = append(sb.rpcDialOpt, grpc.WithTransportCredentials(creds))
		tlsConfig = tlsConf
	} else {
		tlsConfig = nil
		sb.rpcDialOpt = append(sb.rpcDialOpt, grpc.WithTransportCredentials(insecure.NewCredentials()))
	}

	// rpc client dial
	for _, gwHandler := range sb.grpcGatewayHandler {
		if err := gwHandler(ctx, rpcGateway, sb.address, sb.rpcDialOpt); err != nil {
			return err
		}
	}

	// init instance of route
	var route *mux.Router
	if sb.muxRouter == nil {
		route = mux.NewRouter()
	} else {
		route = sb.muxRouter
	}
	// mapping runtime grpc gateway with mux
	route.PathPrefix("/").Handler(rpcGateway)

	// serve http
	var httpHandler http.Handler
	httpHandler = route

	// iterate middleware
	for _, mid := range sb.httpMiddleware {
		httpHandler = mid(httpHandler)
	}

	// insecure server
	if tlsConfig == nil {
		httpServer = &http.Server{
			Addr:    sb.address,
			Handler: routeHandler(httpHandler, sb.rpcServer),
		}
		go sb.rpcServer.Serve(mxRpc)
		go httpServer.Serve(mxRest)
		return multiplexer.Serve()
	}

	// secure server
	httpServer = &http.Server{
		Addr:      sb.address,
		Handler:   routeHandler(httpHandler, sb.rpcServer),
		TLSConfig: tlsConfig,
	}

	return httpServer.Serve(tls.NewListener(listener, httpServer.TLSConfig))
}

// multiplexListener...
func multiplexListener(listener net.Listener) (rpc net.Listener, rest net.Listener, muxMultiplex cmux.CMux, err error) {
	// mux multi plex
	muxMultiPlex := cmux.New(listener)
	rpc = muxMultiPlex.Match(cmux.HTTP2(), cmux.HTTP2HeaderField("content-type", "application/grpc"))
	rest = muxMultiPlex.Match(cmux.HTTP1Fast())
	return rpc, rest, muxMultiPlex, nil
}

// routeHandler....
func routeHandler(other http.Handler, rpc *grpc.Server) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger.Info("[Path] ", r.URL.Path, " [Proto Major] ", r.ProtoMajor)
		if r.ProtoMajor == 2 && strings.HasPrefix(
			r.Header.Get("Content-Type"), "application/grpc") {
			rpc.ServeHTTP(w, r)
		} else {
			other.ServeHTTP(w, r)
		}
	})
}

// register protobuf for grpc health check
func GrpcHealthCheck(server *grpc.Server) {
	healthServer := rpc_health.NewServer()
	// register pb
	healthpb.RegisterHealthServer(server, healthServer)
	for v := range server.GetServiceInfo() {
		healthServer.SetServingStatus(v, healthpb.HealthCheckResponse_SERVING)
	}
	healthServer.SetServingStatus("", healthpb.HealthCheckResponse_SERVING)
}
