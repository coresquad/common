package microservice

import (
	"sync"

	"google.golang.org/grpc"
)

// grpc connector
var (
	singletonGrpcConnector sync.Once
	grpcConnector          *grpc.ClientConn = nil
	err                    error
)

func NewGrpcConnector(target string, opt ...grpc.DialOption) error {
	// once dial to server rpc.
	singletonGrpcConnector.Do(func() {
		client, _err := grpc.Dial(target, opt...)
		if _err != nil {
			err = _err
			return
		}
		// assign value
		grpcConnector = client
	})
	return err
}

// GetGRPCConnector....
//
// get instance of grpc
func GetGRPCConnector() *grpc.ClientConn {
	return grpcConnector
}
