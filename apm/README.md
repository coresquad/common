# How to use

### Prerequisite

if your service running on laptop or local machine not deployed with container/docker, please setup elastichsearch + apm environment

```
export ELASTIC_APM_SERVICE_NAME=geospatial-tracking
export ELASTIC_APM_LOG_LEVEL=DEBUG
export ELASTIC_APM_ENVIRONMENT=development
export ELASTIC_APM_SERVER_URL=http://51.79.155.191:8200
```
After setting APM env you can check service transaction (request,database etc) in kibana.

if your service running on deployed with docker-compose, please setup elastichsearch + apm environment in docker compose file and add env value in field *environment*

```
environment:
		- TZ=Asia/Jakarta
		# elastic apm config
		- ELASTIC_APM_SERVICE_NAME=geospatial-tracking
		- ELASTIC_APM_LOG_LEVEL=DEBUG
		- ELASTIC_APM_ENVIRONMENT=development
		- ELASTIC_APM_SERVER_URL=http://51.79.155.191:8200
```
and add init function in your main file in golang for load system env

```
package main

import (
	"github.com/joho/godotenv"
)

func init() {
	godotenv.Load()
}
func main() {}
```

# Implementation
### Add grpc server tracing with apm

```
import (
	coreapmgrpc "gitlab.com/coresquad/common/apm"
	"google.golang.org/grpc"
)

// Setting or add middleware by grpc request type
opt := coreapmgrpc.GetGrpcServerElasticApmOptions(
		coreapmgrpc.SetUnaryMiddleware(grpc_deliv_mid.UnaryAuthMiddleware()),
		coreapmgrpc.SetStreamMiddleware(grpc_deliv_mid.StreamAuthMiddleware()),
	)

// grpc server instance
server := grpc.NewServer(opt...)
```

### Add grpc dial or client tracing with apm
```
import (
	coreapmgrpc "gitlab.com/coresquad/common/apm"
	"google.golang.org/grpc/credentials/insecure"
)
grpcDialOpt := []grpc.DialOption{}
// if your service not secure or without tls
	grpcDialOpt = append(grpcDialOpt, grpc.WithTransportCredentials(insecure.NewCredentials()))
	svcDeviceConn, err := grpc.Dial(
		YOUR_SERVICE_HOST,
		coreapmgrpc.GetApmGRPcDial(grpcDialOpt...)...,
	)
	if err != nil {
		return nil, err
	}
```
