package apm

import (
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"go.elastic.co/apm/module/apmgrpc"
	gorpc "google.golang.org/grpc"
)

// APM Tracer for gRPC Server...
func GetGrpcServerElasticApmOptions(
	interceptor []gorpc.UnaryServerInterceptor,
	interceptors []gorpc.StreamServerInterceptor,
) []gorpc.ServerOption {
	return []gorpc.ServerOption{
		// middleware for grpc unary request
		grpc_middleware.WithUnaryServerChain(
			getDefaultUnaryOption(interceptor...)...,
		),
		grpc_middleware.WithStreamServerChain(
			getDefaultStreamOption(interceptors...)...,
		),
	}
}

// SetUnaryMiddleware
// set or add middleware for unary request....
func SetUnaryMiddleware(interceptor ...gorpc.UnaryServerInterceptor) []gorpc.UnaryServerInterceptor {
	interceptor = append(interceptor, interceptor...)
	return interceptor
}

// SetStreamMiddleware
func SetStreamMiddleware(interceptors ...gorpc.StreamServerInterceptor) []gorpc.StreamServerInterceptor {
	interceptors = append(interceptors, interceptors...)
	return interceptors
}

// getDefaultStreamOption
// default option for stream interceptor
func getDefaultStreamOption(interceptors ...gorpc.StreamServerInterceptor) []gorpc.StreamServerInterceptor {
	interceptors = append(
		interceptors,
		apmgrpc.NewStreamServerInterceptor(apmgrpc.WithRecovery()),
	)
	return interceptors
}

// getDefaultUnaryOption...
// default option for unary interceptor
func getDefaultUnaryOption(interceptor ...gorpc.UnaryServerInterceptor) []gorpc.UnaryServerInterceptor {
	interceptor = append(
		interceptor,
		apmgrpc.NewUnaryServerInterceptor(apmgrpc.WithRecovery()),
	)

	return interceptor
}

// client apm interceptor
// GetApmGRPcDial...
// apm tracing for gRPC Dial or client
func GetApmGRPcDial(opts ...gorpc.DialOption) []gorpc.DialOption {
	opts = append(opts, gorpc.WithChainUnaryInterceptor(apmgrpc.NewUnaryClientInterceptor()))
	opts = append(opts, gorpc.WithStreamInterceptor(apmgrpc.NewStreamClientInterceptor()))
	return opts
}
