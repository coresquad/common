package mysql

import (
	"database/sql"
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"go.elastic.co/apm/module/apmsql"
	_ "go.elastic.co/apm/module/apmsql/mysql"
)

type (
	MysqlOption struct {
		Host                  string
		Port                  int
		UserName              string
		Password              string
		Name                  string
		Options               []string
		MaxConnection         int
		MaxIdleConnection     int
		MaxConnectionLifeTime int
	}
	Mysql struct {
		dsn           string
		withApmTracer bool
		DBInstance    *sql.DB

		maxConnection         int
		maxIdleConnection     int
		maxConnectionLifeTime int
	}
)

var (
	mysqlInstance  *sql.DB = nil
	errInstance    error
	mysqlSingleton sync.Once
)

func Open(opt MysqlOption) *Mysql {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", opt.UserName, opt.Password, opt.Host, opt.Port, opt.Name)

	if len(opt.Options) > 0 {
		dsn = fmt.Sprintf("%s?%s", dsn, strings.Join(opt.Options, "&"))
	}

	log.Printf("MySQL Connection %s:*****@tcp(%s:%d)/%s?%s", opt.UserName, opt.Host, opt.Port, opt.Name, strings.Join(opt.Options, "&"))

	if opt.MaxConnection == 0 {
		opt.MaxConnection = 10
	}

	if opt.MaxIdleConnection == 0 {
		opt.MaxIdleConnection = 5
	}

	if opt.MaxConnectionLifeTime == 0 {
		opt.MaxConnectionLifeTime = 300
	}

	return &Mysql{
		dsn:                   dsn,
		maxConnection:         opt.MaxConnection,
		maxIdleConnection:     opt.MaxIdleConnection,
		maxConnectionLifeTime: opt.MaxConnectionLifeTime,
	}
}

// with mysql tracer apm
func (m *Mysql) WithApmTracer(value bool) *Mysql {
	m.withApmTracer = value
	return m
}

func (m *Mysql) DB() (*Mysql, error) {
	// handler using tracer or not
	if m.withApmTracer {
		mysqlSingleton.Do(m.openApmDB)
	} else {
		mysqlSingleton.Do(m.openDB)
	}

	if mysqlInstance == nil {
		return nil, errInstance
	}
	// connection pool
	mysqlInstance.SetMaxIdleConns(m.maxIdleConnection)
	mysqlInstance.SetMaxOpenConns(m.maxConnection)
	mysqlInstance.SetConnMaxLifetime(time.Duration(m.maxConnectionLifeTime) * time.Second)

	// assign value
	m.DBInstance = mysqlInstance

	return m, errInstance
}

// openDB initializes a standard MySQL database connection without APM tracing.
func (m *Mysql) openDB() {
	db, err := sql.Open("mysql", m.dsn)
	if err != nil {
		errInstance = err
		return
	}

	if err := db.Ping(); err != nil {
		errInstance = err
		return
	}

	mysqlInstance = db
}

// openApmDB initializes a standard MySQL database connection with APM tracing.
func (m *Mysql) openApmDB() {
	db, err := apmsql.Open("mysql", m.dsn)
	if err != nil {
		errInstance = err
		return
	}
	if err := db.Ping(); err != nil {
		errInstance = err
		return
	}
	mysqlInstance = db
}
func (m *Mysql) Ping() error {
	return m.DBInstance.Ping()
}
func (m *Mysql) GetInstance() *sql.DB {
	return m.DBInstance
}
func (m *Mysql) Close() error {
	return m.DBInstance.Close()
}
