package mysql

import (
	"database/sql"

	"github.com/pressly/goose"
)

type (
	migration struct {
		db   *sql.DB
		path string
		err  error
	}
)

// mysql migration schema
func (m *Mysql) Migration(path string) *migration {
	migrator := &migration{
		db:   m.DBInstance,
		path: path,
	}
	if err := goose.SetDialect("mysql"); err != nil {
		migrator.err = err
		return migrator
	}

	return migrator
}

func (mg *migration) Up() error {
	if mg.err != nil {
		return mg.err
	}
	return goose.Up(mg.db, mg.path)
}

func (mg *migration) Down() error {
	if mg.err != nil {
		return mg.err
	}
	return goose.Down(mg.db, mg.path)
}
