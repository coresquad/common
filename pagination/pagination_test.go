package pagination

import "testing"

func TestGetLimitOffset(t *testing.T) {
	testCases := []struct {
		name string
		arg  Pagination
	}{
		{
			name: "case when page is 2 and perPage is 10",
			arg: Pagination{
				Page:    2,
				PerPage: 10,
				Limit:   10,
				Offset:  10,
			},
		},
		{
			name: "case when page and perpage is empty",
			arg: Pagination{
				Page:    0,
				PerPage: 0,
				Limit:   10,
				Offset:  0,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			pagintation := GetLimitOffset(tc.arg)
			if pagintation.Limit != tc.arg.Limit {
				t.Errorf("Expected limit to be %d but got %d", tc.arg.Limit, pagintation.Limit)
			}
			if pagintation.Offset != tc.arg.Offset {
				t.Errorf("Expected offset to be %d but got %d", tc.arg.Offset, pagintation.Offset)
			}
		})
	}
}
