package pagination

// default value
var (
	Page    = 1
	PerPage = 10
)

type Pagination struct {
	Page    int
	PerPage int
	Limit   int
	Offset  int
}

func GetLimitOffset(pagination Pagination) Pagination {
	// set default page
	if pagination.Page <= 0 {
		pagination.Page = Page
	}

	// set default perPage
	if pagination.PerPage <= 0 {
		pagination.PerPage = PerPage
	}

	offset := (pagination.Page - 1) * pagination.PerPage
	limit := pagination.PerPage
	return Pagination{
		Page:    pagination.Page,
		PerPage: pagination.PerPage,
		Limit:   limit,
		Offset:  offset,
	}
}
