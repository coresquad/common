package pool

import (
	"context"
	"errors"
	"sync"
	"time"

	"github.com/aditya37/logger"
	"github.com/rabbitmq/amqp091-go"
)

var (
	CloseTimeout = 30 * time.Second
)

type RabbitPool struct {
	sync.Mutex
	sync.WaitGroup
	dsn                  string
	connection           *amqp091.Connection
	channel              *amqp091.Channel
	consumers            map[string]Consumer
	notifyClose          chan *amqp091.Error
	notifyChannelClose   chan *amqp091.Error
	notifyDeleteConsumer chan bool
	done                 chan bool
}

// getDSNAddress....
func NewRabbitmq(opt *ClientOption) (*RabbitPool, error) {
	dsn := opt.GetDSN()
	conn, err := amqp091.Dial(dsn)
	if err != nil {
		return nil, err
	}
	channel, err := conn.Channel()
	if err != nil {
		return nil, err
	}

	// notify connection close
	notifyConnection := conn.NotifyClose(make(chan *amqp091.Error, 1))
	notifyChannelClose := channel.NotifyClose(make(chan *amqp091.Error, 1))

	rp := &RabbitPool{
		Mutex:                sync.Mutex{},
		WaitGroup:            sync.WaitGroup{},
		connection:           conn,
		channel:              channel,
		notifyClose:          notifyConnection,
		notifyChannelClose:   notifyChannelClose,
		consumers:            make(map[string]Consumer),
		dsn:                  opt.GetDSN(),
		notifyDeleteConsumer: make(chan bool),
		done:                 make(chan bool),
	}

	// watch event connection close etc...
	go rp.watch()
	return rp, nil
}

// CreateExchange
func (np *RabbitPool) CreateExchange(req CreateExchange) error {
	if err := np.channel.ExchangeDeclare(
		req.Name,
		req.ExchangeType,
		req.Durable,
		req.Autodelete,
		req.Internal,
		req.Nowait,
		req.Args,
	); err != nil {
		return err
	}
	return nil
}

// CreateQueue...
func (np *RabbitPool) CreateQueue(data CreateQueue) (amqp091.Queue, error) {
	return np.channel.QueueDeclare(
		data.Name,
		data.Durable,
		data.Autodelete,
		data.Exclusive,
		data.Nowait,
		data.Args,
	)
}

// QueueBinding...
func (np *RabbitPool) QueueBinding(ctx context.Context, data QueueBinding) error {
	if err := np.channel.QueueBind(
		data.QueueName,
		data.Key,
		data.Exchange,
		data.NoWait,
		data.QueueBindArg,
	); err != nil {
		return err
	}
	return nil
}

// Publish...
func (np *RabbitPool) Publish(ctx context.Context, data RequestPublish) error {
	if err := np.channel.PublishWithContext(
		ctx,
		data.Exchange,
		data.Key,
		data.Mandatory,
		data.Immediate,
		data.Message,
	); err != nil {
		return err
	}

	return nil
}

// PublishAndBindQueue
func (np *RabbitPool) PublishAndBindQueue(ctx context.Context, data RequestPublishAndBindQueue) error {
	// binding queue
	queueBindingPayload := QueueBinding{
		QueueName:    data.QueueName,
		Key:          data.Key,
		Exchange:     data.Exchange,
		NoWait:       data.NoWait,
		QueueBindArg: data.QueueBindArg,
	}
	if err := np.QueueBinding(ctx, queueBindingPayload); err != nil {
		return err
	}

	// publish....
	publishPayload := RequestPublish{
		QueueName: data.QueueName,
		Exchange:  data.Exchange,
		Key:       data.Key,
		Mandatory: data.Mandatory,
		Immediate: data.Immediate,
		Message:   data.Message,
	}
	if err := np.Publish(ctx, publishPayload); err != nil {
		return err
	}
	return nil
}

func (np *RabbitPool) Subscribe(ctx context.Context, consumer Consumer, f ConsumerCallback) error {

	// declare QOS
	if err := np.channel.Qos(consumer.Worker*2, 0, false); err != nil {
		return err
	}

	// consume queue with context...
	msg, err := np.channel.ConsumeWithContext(
		ctx,
		consumer.QueueName,
		consumer.ConsumerId,
		consumer.AutoAck,
		consumer.Exclusive,
		consumer.NoLocal,
		consumer.NoWait,
		consumer.Args,
	)
	if err != nil {
		return err
	}

	// prevent race condition
	np.Mutex.Lock()
	defer np.Mutex.Unlock()

	// create consumer pool and consumer callback function
	np.consumers[consumer.QueueName] = Consumer{
		message:    msg,
		callback:   f,
		Worker:     consumer.Worker,
		QueueName:  consumer.QueueName,
		ConsumerId: consumer.ConsumerId,
		AutoAck:    consumer.AutoAck,
		Exclusive:  consumer.Exclusive,
		NoLocal:    consumer.NoLocal,
		NoWait:     consumer.NoWait,
		Args:       consumer.Args,
	}
	// create goroutine for listen event from message
	go np.consumeMessage(consumer.QueueName)
	return nil
}

// consumeMessage
// consume message and assign to callback function
// from consumer pool
func (np *RabbitPool) consumeMessage(name string) {
	consumer := np.consumers[name]
	worker := NewWorkerPool(consumer.Worker)
	// iterate message
	for msg := range consumer.message {
		if len(msg.Body) == 0 {
			msg.Ack(false)
			return
		}
		nWorker := worker.Get()
		// add wait group...
		np.WaitGroup.Add(1)

		// assign value to callback function
		go func(event amqp091.Delivery, nworker int) {
			logger.Info("Consume Message with ", "[Correlation ID] ", event.CorrelationId, " [Worker] ", nworker)
			// callback function
			ctx := context.Background()
			consumer.callback(ctx, event)
			// wait group done
			defer func() {
				np.WaitGroup.Done()
				worker.Put(nworker)
			}()
		}(msg, nWorker)
	}
}

// watch
// watch event from rabbitmq like closed connection, will auto reconnect
func (np *RabbitPool) watch() {
	for {
		select {
		case notif, ok := <-np.notifyClose:
			if ok {
				logger.Info("Detected closed channel with code ", notif.Code, " Reason ", notif.Reason)
				if err := np.reconnect(); err != nil {
					logger.Error("reconnect failed reason ", err)
					break
				}
			}
		case notifChannel, ok := <-np.notifyChannelClose:
			if ok {
				logger.Info("Detected closed channel with code ", notifChannel.Code, " Reason ", notifChannel.Reason)
				newCh, err := np.connection.Channel()
				if err != nil {
					logger.Error("reconnect failed reason ", err)
					break
				}
				if err := np.reconnectChannel(newCh); err != nil {
					logger.Error("reconnect failed reason ", err)
					break
				}
			}
		case closing := <-np.notifyDeleteConsumer:
			if closing {
				// deleting consumer pool
				logger.Info("Closing ", len(np.consumers), " Consumers")
				for consumer := range np.consumers {
					// close consumer
					np.channel.Cancel(consumer, false)
					// delete consumer from pool
					delete(np.consumers, consumer)
				}
				// notify done and close all connection
				np.done <- true
			}
		}
	}
}

// reconnect
// reconnect if detected connection is closed
func (np *RabbitPool) reconnect() error {
	conn, err := amqp091.Dial(np.dsn)
	if err != nil {
		return err
	}

	// assign new instance for connection
	// after close
	np.connection = conn
	// recreate channel after success re-connect
	np.notifyClose = conn.NotifyClose(make(chan *amqp091.Error, 1))

	return nil
}

// reconnectChannel
// reconnect channel and re init subscribe
func (np *RabbitPool) reconnectChannel(channel *amqp091.Channel) error {
	if np.connection.IsClosed() {
		return errors.New("failed to reconnect channel, connection is closed")
	}
	// assign new channel instance from connection
	np.channel = channel
	// recreate channel after success re-connect
	np.notifyChannelClose = channel.NotifyClose(make(chan *amqp091.Error, 1))

	np.Mutex.Lock()
	// re consume message after success reconnect to channel
	if err := np.reconsumeMessage(); err != nil {
		return err
	}
	np.Mutex.Unlock()

	return nil
}

// reconsumeMessage
// reconsume message after reconnect channel
func (np *RabbitPool) reconsumeMessage() error {
	ctx := context.Background()
	for key, val := range np.consumers {
		msg, err := np.channel.ConsumeWithContext(
			ctx,
			val.QueueName,
			val.ConsumerId,
			val.AutoAck,
			val.Exclusive,
			val.NoLocal,
			val.NoWait,
			val.Args,
		)
		if err != nil {
			logger.Error(err)
			return err
		}
		np.consumers[key] = Consumer{
			message:    msg,
			callback:   val.callback,
			Worker:     val.Worker,
			QueueName:  val.QueueName,
			ConsumerId: val.ConsumerId,
			AutoAck:    val.AutoAck,
			Exclusive:  val.Exclusive,
			NoLocal:    val.NoLocal,
			NoWait:     val.NoWait,
			Args:       val.Args,
		}
		go np.consumeMessage(key)
	}
	return nil
}

// Close...
func (np *RabbitPool) Close(ctx context.Context) error {
	// for graceful shutdown
	_, cancel := context.WithTimeout(ctx, CloseTimeout)
	defer cancel()

	// notify delete consumer
	np.notifyDeleteConsumer <- true
	// blocking or wait until done on remove existin consumer
	<-np.done

	// do close all connection
	if err := np.connection.Close(); err != nil {
		return err
	}
	if err := np.channel.Close(); err != nil {
		return err
	}

	return nil
}
