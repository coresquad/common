package pool

import "fmt"

type ClientOption struct {
	Port     int
	Username string
	Password string
	Host     string
}

func NewClientOption() *ClientOption {
	return &ClientOption{}
}

func (co *ClientOption) SetPort(val int) *ClientOption {
	if val == 0 {
		co.Port = 5672
		return co
	}
	co.Port = val
	return co
}
func (co *ClientOption) SetHost(val string) *ClientOption {
	if val != "" {
		co.Host = val
		return co
	}
	co.Host = "127.0.01"
	return co
}
func (co *ClientOption) SetUsername(val string) *ClientOption {
	co.Username = val
	return co
}
func (co *ClientOption) SetPassword(val string) *ClientOption {
	co.Password = val
	return co
}
func (co *ClientOption) GetDSN() string {
	if co.Host == "" {
		co.Host = "127.0.0.1"
	}
	if co.Port <= 0 {
		co.Port = 5672
	}
	return fmt.Sprintf("amqp://%s:%s@%s:%d/", co.Username, co.Password, co.Host, co.Port)
}
