package pool

import "sync"

type workerPool struct {
	sync.Mutex
	workerNum chan int
}

func NewWorkerPool(worker int) *workerPool {
	pp := &workerPool{
		Mutex:     sync.Mutex{},
		workerNum: make(chan int, worker),
	}
	pp.init()
	return pp
}

// set first value to worker channel
func (np *workerPool) init() {
	for i := 0; i < cap(np.workerNum); i++ {
		np.workerNum <- i
	}
}

// get current worker number or value
func (np *workerPool) Get() int {
	val := <-np.workerNum
	return val
}

// set value to worker channel
// for prevent deadlock channel
func (np *workerPool) Put(val int) {
	np.Lock()
	defer np.Unlock()
	np.workerNum <- val
}

// clear...
func (np *workerPool) Clear() {
	np.workerNum <- -1
}
