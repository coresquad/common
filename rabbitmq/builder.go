package pool

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/rabbitmq/amqp091-go"
)

type ConsumerCallback func(ctx context.Context, msg amqp091.Delivery) error

type (
	RequestPublish struct {
		QueueName string
		Exchange  string
		Key       string
		Mandatory bool
		Immediate bool
		Message   amqp091.Publishing
	}
	QueueBinding struct {
		QueueName    string
		Exchange     string
		Key          string
		NoWait       bool
		QueueBindArg amqp091.Table
	}
	CreateExchange struct {
		Name         string
		ExchangeType string
		RoutingKey   string
		Durable      bool
		Autodelete   bool
		Internal     bool
		Nowait       bool
		Exclusive    bool
		Args         amqp091.Table
	}
	CreateQueue struct {
		Name       string
		Durable    bool
		Autodelete bool
		Nowait     bool
		Exclusive  bool
		Args       amqp091.Table
	}

	// config subscribe
	Consumer struct {
		message    <-chan amqp091.Delivery
		callback   ConsumerCallback
		Worker     int
		QueueName  string
		ConsumerId string
		AutoAck    bool
		Exclusive  bool
		NoLocal    bool
		NoWait     bool
		Args       amqp091.Table
	}

	// RequestPublishAndBindQueue
	// request payload or data for
	RequestPublishAndBindQueue struct {
		QueueName    string
		Exchange     string
		Key          string
		Mandatory    bool
		Immediate    bool
		NoWait       bool
		Message      amqp091.Publishing
		QueueBindArg amqp091.Table
	}
)

// Publish Builder
// builder for publish message
func NewPublishBuilder() *RequestPublish { return &RequestPublish{} }

// SetQueueName
func (rb *RequestPublish) SetQueueName(val string) *RequestPublish {
	rb.QueueName = val
	return rb
}

// SetExchange
func (rb *RequestPublish) SetExchange(val string) *RequestPublish {
	rb.Exchange = val
	return rb
}

// SetRoutingKey
func (rb *RequestPublish) SetRoutingKey(val string) *RequestPublish {
	rb.Key = val
	return rb
}

// IsMandatory
func (rb *RequestPublish) IsMandatory(val bool) *RequestPublish {
	rb.Mandatory = val
	return rb
}

// IsImmediate
func (rb *RequestPublish) IsImmediate(val bool) *RequestPublish {
	rb.Immediate = val
	return rb
}

// SetMessage...
// correlation id is null, will generate default correlation id with prefix correlation.{some_uuid}
// timestamp null or isZero, will set timestamp with current timestamp
func (rb *RequestPublish) SetMessage(val amqp091.Publishing) *RequestPublish {
	// random message and correlation id
	msgId := uuid.New().String()

	// generate random or default correlation id
	if val.CorrelationId == "" {
		val.CorrelationId = fmt.Sprintf("correlation.%s", msgId)
	}

	// set default value timestamp if timestamp null
	// with current timestamp
	if val.Timestamp.IsZero() {
		val.Timestamp = time.Now()
	}

	// set default message id
	if val.MessageId == "" {
		val.MessageId = fmt.Sprintf("msg.%s", msgId)
	}

	rb.Message = val
	return rb
}

// Build
func (rb *RequestPublish) Build() RequestPublish {
	return RequestPublish{
		QueueName: rb.QueueName,
		Exchange:  rb.Exchange,
		Key:       rb.Key,
		Mandatory: rb.Mandatory,
		Immediate: rb.Immediate,
		Message:   rb.Message,
	}
}

// NewQueueBinding...
func NewQueueBindingBuilder() *QueueBinding { return &QueueBinding{} }

// SetQueueName
func (qb *QueueBinding) SetQueueName(val string) *QueueBinding {
	qb.QueueName = val
	return qb
}

// SetExchange
func (qb *QueueBinding) SetExchange(val string) *QueueBinding {
	qb.Exchange = val
	return qb
}

func (qb *QueueBinding) SetRoutingKey(val string) *QueueBinding {
	qb.Key = val
	return qb
}

// NoWait
func (qb *QueueBinding) SetNoWait(val bool) *QueueBinding {
	qb.NoWait = val
	return qb
}
func (qb *QueueBinding) SetArgs(val map[string]interface{}) *QueueBinding {
	qb.QueueBindArg = val
	return qb
}

// Build
func (qb *QueueBinding) Build() QueueBinding {
	return QueueBinding{
		QueueName:    qb.QueueName,
		Exchange:     qb.Exchange,
		Key:          qb.Key,
		NoWait:       qb.NoWait,
		QueueBindArg: qb.QueueBindArg,
	}
}

// CreateExchange
func NewExchangeBuilder() *CreateExchange { return &CreateExchange{} }

// SetExchangeName
func (eb *CreateExchange) SetExchangeName(val string) *CreateExchange {
	eb.Name = val
	return eb
}

// SetExchangeType
// set exchange type, default exchage type is "topic"
func (eb *CreateExchange) SetExchangeType(val string) *CreateExchange {
	if val == "" {
		val = "topic"
	}
	eb.ExchangeType = val
	return eb
}

// SetRoutingKey
func (eb *CreateExchange) SetRoutingKey(val string) *CreateExchange {
	eb.RoutingKey = val
	return eb
}

// IsDurable
func (eb *CreateExchange) IsDurable(val bool) *CreateExchange {
	eb.Durable = val
	return eb
}

// IsAutodelete
func (eb *CreateExchange) IsAutodelete(val bool) *CreateExchange {
	eb.Autodelete = val
	return eb
}

// IsInternal
func (eb *CreateExchange) IsInternal(val bool) *CreateExchange {
	eb.Internal = val
	return eb
}

// IsNowait
func (eb *CreateExchange) IsNowait(val bool) *CreateExchange {
	eb.Nowait = val
	return eb
}

// IsExclusive
func (eb *CreateExchange) IsExclusive(val bool) *CreateExchange {
	eb.Exclusive = val
	return eb
}

// SetArgs
func (eb *CreateExchange) SetArgs(val amqp091.Table) *CreateExchange {
	eb.Args = val
	return eb
}

// Build
func (eb *CreateExchange) Build() CreateExchange {
	if eb.ExchangeType == "" {
		eb.ExchangeType = "topic"
	}
	return CreateExchange{
		Name:         eb.Name,
		ExchangeType: eb.ExchangeType,
		RoutingKey:   eb.RoutingKey,
		Durable:      eb.Durable,
		Autodelete:   eb.Autodelete,
		Internal:     eb.Internal,
		Nowait:       eb.Nowait,
		Exclusive:    eb.Exclusive,
		Args:         eb.Args,
	}
}

// CreateQueue
// builder for create queue
func NewQueue() *CreateQueue { return &CreateQueue{} }

// SetName
func (cq *CreateQueue) SetName(val string) *CreateQueue {
	cq.Name = val
	return cq
}

// IsDurable
func (cq *CreateQueue) IsDurable(val bool) *CreateQueue {
	cq.Durable = val
	return cq
}

// IsAutodelete
func (cq *CreateQueue) IsAutodelete(val bool) *CreateQueue {
	cq.Autodelete = val
	return cq
}

// IsNowait
func (cq *CreateQueue) IsNowait(val bool) *CreateQueue {
	cq.Nowait = val
	return cq
}

// IsExclusive
func (cq *CreateQueue) IsExclusive(val bool) *CreateQueue {
	cq.Exclusive = val
	return cq
}

// SetArgs
func (cq *CreateQueue) SetArgs(val amqp091.Table) *CreateQueue {
	cq.Args = val
	return cq
}

// Build
func (cq *CreateQueue) Build() CreateQueue {
	return CreateQueue{
		Name:       cq.Name,
		Durable:    cq.Durable,
		Autodelete: cq.Autodelete,
		Nowait:     cq.Nowait,
		Exclusive:  cq.Exclusive,
		Args:       cq.Args,
	}
}
